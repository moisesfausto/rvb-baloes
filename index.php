<?php include_once('header.php'); ?>


	<div id="main-wrapper">
		<div class="site-wrapper-reveal">
			<!--============ Appointment Hero Start ============-->

			<div class="main_ts_carousel">
				<div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
						<!-- <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
						<li data-target="#carouselExampleCaptions" data-slide-to="2"></li> -->
					</ol>
					<div class="carousel-inner">
						<div class="carousel-item active">
							<div class="banner_carousel" style="background-image: url(assets/images/banners/banner.png);"
								alt="promocionais"></div>

							<div class="carousel-form d-none d-md-block">
								<div class="container">
									<div class="row">
										<div class="col-lg-4">
											<!-- formulario aqui -->
										</div>
									</div>
								</div>
							</div>


						</div>
					</div>
				</div>
			</div>
			<!--============ Appointment Hero End ============-->

			<?php include_once('partials/diferenciais.php'); ?>

			<!--===========  breve informação Start =============-->
			<div class="main_info">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<h2>RVB Balões, com 21 anos de experiência, entende e ajuda nossos clientes em suas ações de marketing e
								vendas. ara nós, é indispensável, excelência no atendimento, agilidade na entrega e qualidade de
								verdade.</h2>
							<ul>
								<li>
									<img src="assets/images/icons/informacao/tecido-de-qualidade.svg" alt="Tecido de qualidade">
									<span>TECIDO DE QUALIDADE</span>
								</li>
								<li>
									<img src="assets/images/icons/informacao/empresa-ecologica.svg" alt="Empresa ecologica">
									<span>EMPRESA ECÓLOGICA</span>
								</li>
								<li>
									<img src="assets/images/icons/informacao/hp.svg" alt="HP">
									<span>IMPRESSÃO LATEX</span>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!--===========  breve informação end =============-->

			<!--===========  modelos inflaveis start =============-->
			<div class="main_modelos_inflaveis">
				<div class="container">
					<h2 class="wow move-up">Modelos dos nossos Infláveis</h2>
					<div class="row mb-5">
						<div
							class="col-sm-12 col-md-6 col-lg-3 text-center d-flex justify-content-between flex-column align-items-center">
							<a href=""><img src="assets/images/modelos-inflaveis/tunil-inflavel.png" class="img-fluid"
									alt="Túnel Inflável"></a>
							<span class="mb-3">Túnel Inflável</span>
						</div>
						<div
							class="col-sm-12 col-md-6 col-lg-3 text-center d-flex justify-content-between flex-column align-items-center">
							<a href=""><img src="assets/images/modelos-inflaveis/tenda-inflavel.png" class="img-fluid"
									alt="Tenda Inflável"></a>
							<span class="mb-3">Tenda Inflável</span>
						</div>
						<div
							class="col-sm-12 col-md-6 col-lg-3 text-center d-flex justify-content-between flex-column align-items-center">
							<a href=""><img src="assets/images/modelos-inflaveis/balao-promocional.png" class="img-fluid"
									alt="Balão Promocional"></a>
							<span class="mb-3">Balão Promocional</span>
						</div>
						<div
							class="col-sm-12 col-md-6 col-lg-3 text-center d-flex justify-content-between flex-column align-items-center">
							<a href=""><img src="assets/images/modelos-inflaveis/portal-inflavel.png" class="img-fluid"
									alt="Portal Inflável"></a>
							<span class="mb-3">Portal Inflável</span>
						</div>
					</div>

					<div class="row mb-5">
						<div
							class="col-sm-12 col-md-6 col-lg-3 text-center d-flex justify-content-between flex-column align-items-center">
							<a href=""><img src="assets/images/modelos-inflaveis/painel-inflavel.png" class="img-fluid"
									alt="Painel Inflável"></a>
							<span class="mb-3">Painel Inflável</span>
						</div>
						<div
							class="col-sm-12 col-md-6 col-lg-3 text-center d-flex justify-content-between flex-column align-items-center">
							<a href=""><img src="assets/images/modelos-inflaveis/totem-inflavel.png" class="img-fluid"
									alt="Totem Inflável"></a>
							<span class="mb-3">Totem Inflável</span>
						</div>
						<div
							class="col-sm-12 col-md-6 col-lg-3 text-center d-flex justify-content-between flex-column align-items-center">
							<a href=""><img src="assets/images/modelos-inflaveis/logomarca-inflavel.png" class="img-fluid"
									alt="Logomarca Inflável"></a>
							<span class="mb-3">Logomarca Inflável</span>
						</div>
						<div
							class="col-sm-12 col-md-6 col-lg-3 text-center d-flex justify-content-between flex-column align-items-center">
							<a href=""><img src="assets/images/modelos-inflaveis/chute-a-gol-inflavel.png" class="img-fluid"
									alt="Chute a Gol Inflável"></a>
							<span class="mb-3">Chute a Gol Inflável</span>
						</div>
					</div>

					<div class="row mb-5">
						<div
							class="col-sm-12 col-md-6 col-lg-3 text-center d-flex justify-content-between flex-column align-items-center">
							<a href=""><img src="assets/images/modelos-inflaveis/potes-inflaveis.png" class="img-fluid"
									alt="Potes Infláveis"></a>
							<span class="mb-3">Potes Infláveis</span>
						</div>
						<div
							class="col-sm-12 col-md-6 col-lg-3 text-center d-flex justify-content-between flex-column align-items-center">
							<a href=""><img src="assets/images/modelos-inflaveis/garrafa-e-latas-inflaveis.png" class="img-fluid"
									alt="Garrafas e Latas Infláveis"></a>
							<span class="mb-3">Garrafas e Latas Infláveis</span>
						</div>
						<div
							class="col-sm-12 col-md-6 col-lg-3 text-center d-flex justify-content-between flex-column align-items-center">
							<a href=""><img src="assets/images/modelos-inflaveis/mascote-e-bonecos-inflaveis.png" class="img-fluid"
									alt="Mascotes e Bonecos Infláveis"></a>
							<span class="mb-3">Mascotes e Bonecos Infláveis</span>
						</div>
						<div
							class="col-sm-12 col-md-6 col-lg-3 text-center d-flex justify-content-between flex-column align-items-center">
							<a href=""><img src="assets/images/modelos-inflaveis/stand-inflavel.png" class="img-fluid"
									alt="Stand Inflável"></a>
							<span class="mb-3">Stand Inflável</span>
						</div>
					</div>

					<p>Destaque-se em seu Negócio ou Evento com Infláveis Promocionais. <a href=""
							class="text-decoration-none">Solicite seu Orçamento!</a></p>
				</div>
			</div>
			<!--===========  modelos inflaveis end =============-->

			<!--===========  feature-large-images-wrapper  Start =============-->
			<div class="main_galeria_de_imagens">
				<div class="feature-large-images-wrapper section-space--ptb_100">
					<div class="container">

						<div class="row">
							<div class="col-lg-12">
								<!-- section-title-wrap Start -->
								<div class="section-title-wrap text-center section-space--mb_30">
									<h6 class="section-sub-title mb-20">GALERIA DE IMAGENS</h6>
									<h3 class="heading">Nossos resultados são a demonstração <br> de toda atenção dedicação.</h3>
								</div>
								<!-- section-title-wrap Start -->
							</div>
						</div>

						<div class="row">
							<div class="col-12">
								<div class="row small-mt__30">

									<div class="col-lg-3 col-md-6 mt-30">
										<!-- Box large image warap Start -->
										<a href="#" class="box-large-image__two">
											<div class="box-large-image__two__box">
												<div class="box-large-image__midea">
													<div class="single-gallery__thum bg-item-images bg-img"
														data-bg="assets/images/box-image/droga-raia.jpg"></div>
												</div>

												<div class="box-info">
													<h5 class="heading">DrogaRaia</h5>
													<!-- <div class="box-more-info">
														<div class="text">We provide the most responsive and functional IT design for companies and
															businesses worldwide.</div>
														<div class="btn">
															<i class="button-icon far fa-long-arrow-right"></i>
														</div>
													</div> -->
												</div>
											</div>
										</a>
										<!-- Box large image warap End -->
									</div>

									<div class="col-lg-6 col-md-6  mt-30">
										<!-- Box large image warap Start -->
										<a href="#" class="box-large-image__two">
											<div class="box-large-image__two__box">
												<div class="box-large-image__midea">
													<div class="single-gallery__thum bg-item-images bg-img"
														data-bg="assets/images/box-image/wizard.jpg"></div>
												</div>

												<div class="box-info">
													<h5 class="heading">Herbalife</h5>
												</div>
											</div>
										</a>
										<!-- Box large image warap End -->
									</div>

									<div class="col-lg-3 col-md-6 mt-30">
										<!-- Box large image warap Start -->
										<a href="#" class="box-large-image__two">
											<div class="box-large-image__two__box">
												<div class="box-large-image__midea">
													<div class="single-gallery__thum bg-item-images bg-img"
														data-bg="assets/images/box-image/herbalife.jpg"></div>
												</div>

												<div class="box-info">
													<h5 class="heading">Herbalife</h5>
												</div>
											</div>
										</a>
										<!-- Box large image warap End -->
									</div>

									<div class="col-lg-4 col-md-6 mt-30">
										<!-- Box large image warap Start -->
										<a href="#" class="box-large-image__two">
											<div class="box-large-image__two__box">
												<div class="box-large-image__midea">
													<div class="single-gallery__thum bg-item-images bg-img"
														data-bg="assets/images/box-image/now.jpg"></div>
												</div>

												<div class="box-info">
													<h5 class="heading">Net Now</h5>
												</div>
											</div>
										</a>
										<!-- Box large image warap End -->
									</div>

									<div class="col-lg-4 col-md-6 mt-30">
										<!-- Box large image warap Start -->
										<a href="#" class="box-large-image__two">
											<div class="box-large-image__two__box">
												<div class="box-large-image__midea">
													<div class="single-gallery__thum bg-item-images bg-img"
														data-bg="assets/images/box-image/ciesp.jpg"></div>
												</div>

												<div class="box-info">
													<h5 class="heading">Ciesp</h5>
												</div>
											</div>
										</a>
										<!-- Box large image warap End -->
									</div>

									<div class="col-lg-4 col-md-6 mt-30">
										<!-- Box large image warap Start -->
										<a href="#" class="box-large-image__two">
											<div class="box-large-image__two__box">
												<div class="box-large-image__midea">
													<div class="single-gallery__thum bg-item-images bg-img"
														data-bg="assets/images/box-image/fiat.jpg"></div>
												</div>

												<div class="box-info">
													<h5 class="heading">Fiat</h5>
												</div>
											</div>
										</a>
										<!-- Box large image warap End -->
									</div>

								</div>
								<div class="col-lg-12">
									<div class="feature-list-button-box mt-40 text-center">
										<a href="#" class="ht-btn ht-btn-md">Veja Mais</a>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
			<!--===========  feature-large-images-wrapper  End =============-->

			<!--====================  testimonial section ====================-->
			<div class="main_satisfacao">
				<div class="testimonial-slider-area section-space--ptb_100">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<div class="section-title-wrap text-center section-space--mb_40">
									<h6 class="section-sub-title mb-20">SATISFAÇÃO</h6>
									<h3 class="heading">Depoimentos do clientes!</h3>
								</div>
								<div class="testimonial-slider">
									<div class="swiper-container testimonial-slider__container">
										<div class="swiper-wrapper testimonial-slider__wrapper">


											<div class="swiper-slide">
												<div class="testimonial-slider__one wow move-up">

													<div class="testimonial-slider--info">
														<div class="testimonial-slider__media">
															<img src="https://via.placeholder.com/90x90" class="img-fluid"
																alt="Larissa">
														</div>

														<div class="testimonial-slider__author">
															<div class="testimonial-rating">
																<span class="fa fa-star"></span>
																<span class="fa fa-star"></span>
																<span class="fa fa-star"></span>
																<span class="fa fa-star"></span>
																<span class="fa fa-star"></span>
															</div>
															<div class="author-info">
																<h6 class="name">Larissa Oliveira</h6>
																<span class="designation">Marketing Grupo Bandeirantes</span>
															</div>
														</div>
													</div>

													<div class="testimonial-slider__text">“Gostaria de agradecer por toda a atenção no atendimento desde o orçamento até o trabalho de pós-venda. Nossas tendas ficaram ótimas, atenderam às nossas necessidades e expectativas.</div>

												</div>
											</div>


											<div class="swiper-slide">
												<div class="testimonial-slider__one wow move-up">

													<div class="testimonial-slider--info">
														<div class="testimonial-slider__media">
															<img src="https://via.placeholder.com/90x90" class="img-fluid"
																alt="Leandro">
														</div>

														<div class="testimonial-slider__author">
															<div class="testimonial-rating">
																<span class="fa fa-star"></span>
																<span class="fa fa-star"></span>
																<span class="fa fa-star"></span>
																<span class="fa fa-star"></span>
																<span class="fa fa-star"></span>
															</div>
															<div class="author-info">
																<h6 class="name">Leandro Lima</h6>
																<span class="designation">Diretor Regional Farma Conde</span>
															</div>
														</div>
													</div>

													<div class="testimonial-slider__text">“Gostaria de agradecer por toda a atenção no atendimento desde o orçamento até o trabalho de pós-venda. Nossas tendas ficaram ótimas, atenderam às nossas necessidades e expectativas.</div>

												</div>
											</div>


										</div>
										<div class="swiper-pagination swiper-pagination-t01 section-space--mt_30"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--====================  End of testimonial section  ====================-->


			<?php include_once('partials/balonismo.php'); ?>


			<!--===========  Blog Start =============-->
			<div class="blog-section-wrapper section-space--pt_100 bg-theme-four section-space--pb_70">
				<div class="container">

					<div class="row">

						<div class="col-lg-4 col-md-6 wow move-up animated animated" style="visibility: visible;">
							<!--======= Single Blog Item Start ========-->
							<div class="single-blog-item blog-grid">
								<!-- Post Feature Start -->
								<div class="post-feature blog-thumbnail">
									<a href="blog-post-layout-one.html">
										<img class="img-fluid" src="https://via.placeholder.com/370x230" alt="Blog Images">
									</a>
								</div>
								<!-- Post Feature End -->

								<!-- Post info Start -->
								<div class="post-info lg-blog-post-info">
									<div class="post-meta">
										<div class="post-date">
											<span class="far fa-calendar meta-icon"></span>
											Outubro 29, 2020
										</div>
									</div>

									<h5 class="post-title font-weight--bold">
										<a href="#">Qual o consumo energético de um inflável promocional?</a>
									</h5>

									<div class="post-excerpt mt-15">
										<p>“Gostaria de agradecer por toda a atenção no atendimento desde o orçamento até o trabalho de pós-venda. Nossas tendas ficaram ótimas...</p>
									</div>
									<div class="btn-text">
										<a href="#">Leia mais <i class="ml-1 button-icon fas fa-chevron-right"></i></a>
									</div>
								</div>
								<!-- Post info End -->
							</div>
							<!--===== Single Blog Item End =========-->

						</div>

						<div class="col-lg-4 col-md-6 wow move-up animated animated" style="visibility: visible;">
							<!--======= Single Blog Item Start ========-->
							<div class="single-blog-item blog-grid">
								<!-- Post Feature Start -->
								<div class="post-feature blog-thumbnail">
									<a href="blog-post-layout-one.html">
										<img class="img-fluid" src="https://via.placeholder.com/370x230" alt="Blog Images">
									</a>
								</div>
								<!-- Post Feature End -->

								<!-- Post info Start -->
								<div class="post-info lg-blog-post-info">
									<div class="post-meta">
										<div class="post-date">
											<span class="far fa-calendar meta-icon"></span>
											Outubro 29, 2020
										</div>
									</div>

									<h5 class="post-title font-weight--bold">
										<a href="#">Qual o consumo energético de um inflável promocional?</a>
									</h5>

									<div class="post-excerpt mt-15">
										<p>“Gostaria de agradecer por toda a atenção no atendimento desde o orçamento até o trabalho de pós-venda. Nossas tendas ficaram ótimas...</p>
									</div>
									<div class="btn-text">
										<a href="#">Leia mais <i class="ml-1 button-icon fas fa-chevron-right"></i></a>
									</div>
								</div>
								<!-- Post info End -->
							</div>
							<!--===== Single Blog Item End =========-->

						</div>

						<div class="col-lg-4 col-md-6 wow move-up animated animated" style="visibility: visible;">
							<!--======= Single Blog Item Start ========-->
							<div class="single-blog-item blog-grid">
								<!-- Post Feature Start -->
								<div class="post-feature blog-thumbnail">
									<a href="blog-post-layout-one.html">
										<img class="img-fluid" src="https://via.placeholder.com/370x230" alt="Blog Images">
									</a>
								</div>
								<!-- Post Feature End -->

								<!-- Post info Start -->
								<div class="post-info lg-blog-post-info">
									<div class="post-meta">
										<div class="post-date">
											<span class="far fa-calendar meta-icon"></span>
											Outubro 29, 2020
										</div>
									</div>

									<h5 class="post-title font-weight--bold">
										<a href="#">Qual o consumo energético de um inflável promocional?</a>
									</h5>

									<div class="post-excerpt mt-15">
										<p>“Gostaria de agradecer por toda a atenção no atendimento desde o orçamento até o trabalho de pós-venda. Nossas tendas ficaram ótimas...</p>
									</div>
									<div class="btn-text">
										<a href="#">Leia mais <i class="ml-1 button-icon fas fa-chevron-right"></i></a>
									</div>
								</div>
								<!-- Post info End -->
							</div>
							<!--===== Single Blog Item End =========-->

						</div>

					</div>
				</div>
			</div>
			<!--===========  Blog End =============-->


<?php include_once('footer.php'); ?>
