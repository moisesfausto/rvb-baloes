<?php require_once('header.php'); ?>
<?php include_once('partials/diferenciais.php'); ?>

<!-- breadcrumb-area start -->
<div class="breadcrumb-area main_ts_bg_contato">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="breadcrumb_box text-center">
					<h2 class="breadcrumb-title">Contato</h2>
					<!-- breadcrumb-list start -->
					<ul class="breadcrumb-list">
						<li class="breadcrumb-item"><a href="index.html">HOME</a></li>
						<li class="breadcrumb-item active">CONTATO </li>
					</ul>
					<!-- breadcrumb-list end -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- breadcrumb-area end -->




<div id="main-wrapper">
	<div class="site-wrapper-reveal">
		<!--====================  Conact us Section Start ====================-->
		<div class="contact-us-section-wrappaer section-space--pt_100 section-space--pb_70">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-6 col-lg-6">
						<div class="conact-us-wrap-one mb-30">
							<div class="sub-heading">FORMULÁRIO DE CONTATO</div>
							<h3 class="heading">Entre em contato conosco através do formulário ao lado ou se preferir pelo nosso tele-atendimento.</h3>

						</div>
					</div>

					<div class="col-lg-6 col-lg-6">
						<div class="contact-form-wrap">

							<!-- <form id="contact-form" action="http://whizthemes.com/mail-php/jowel/mitech/php/mail.php" method="post"> -->
							<form id="contact-form" action="assets/php/mail.php" method="post">
								<div class="contact-form">
									<div class="contact-input">
										<div class="contact-inner">
											<label for="">Nome*</label>
											<input name="con_name" type="text">
										</div>
										<div class="contact-inner">
											<label for="">E-mail*</label>
											<input name="con_email" type="email">
										</div>
									</div>
									<div class="contact-inner">
										<label for="">Assunto</label>
										<input name="con_subject" type="text">
									</div>
									<div class="contact-inner contact-message">
										<label for="">Mensagem</label>
										<textarea name="con_message"></textarea>
									</div>
									<div class="submit-btn mt-20">
										<button class="ht-btn ht-btn-md bg-spotlight" type="submit">ENVIAR FORMULÁRIO DE CONTATO <i class="far fa-chevron-circle-right"></i></button>
										<p class="form-messege"></p>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--====================  Conact us Section End  ====================-->

	</div>

	<div class="main_ts_maps">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.8943962784583!2d-46.53734858444547!3d-23.536300366553768!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5e14c991bf95%3A0x284efe8b3f98034b!2sR.%20Mois%C3%A9s%20Marx%2C%201093%20-%20Vila%20Matilde%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2003507-000!5e0!3m2!1spt-BR!2sbr!4v1618166677165!5m2!1spt-BR!2sbr" width="100%" height="543" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
	</div>




	<?php include('partials/newsletter.php'); ?>
	<?php require_once('footer.php'); ?>
