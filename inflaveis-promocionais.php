<?php require_once('header.php'); ?>
<?php include_once('partials/diferenciais.php'); ?>

<!-- breadcrumb-area start -->
<div class="breadcrumb-area main_ts_inflaveis_promocionais">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="breadcrumb_box text-center">
					<h2 class="breadcrumb-title">Infláveis Promocionais</h2>
					<!-- breadcrumb-list start -->
					<ul class="breadcrumb-list">
						<li class="breadcrumb-item"><a href="index.html">Home</a></li>
						<li class="breadcrumb-item active">INFLÁVEIS PROMOCIONAIS</li>
					</ul>
					<!-- breadcrumb-list end -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- breadcrumb-area end -->

<div id="main-wrapper">
	<div class="site-wrapper-reveal">

		<!--===========  feature-images-wrapper  Start =============-->
		<div class="feature-images-wrapper section-space--ptb_100 main_inflaveis_promocionais_descricao">
			<div class="container">

				<div class="row">
					<div class="col-lg-12">
						<!-- section-title-wrap Start -->
						<div class="section-title-wrap text-center section-space--mb_10">
							<h3 class="heading">Infláveis <span class="text-color-spotlight">RVB</span>, gigantes como sua <span class="text-color-spotlight">Marca!</span></h3>
						</div>
						<!-- section-title-wrap Start -->
						<div class="section-descricao">
							<h3>O que são Infláveis Promocionais?</h3>
							<p>Infláveis promocionais são um tipo de propaganda versátil de simples montagem e com um excelente impacto visual.
								Pode ser criada em praticamente qualquer formato e no tamanho, entre 2 a 30 metros de altura.
								Já vem com um moto ventilador embutido, para manter os infláveis perfeitamente cheios e com iluminação em alguns modelos.</p>

							<p>Os Infláveis da RVB Balões são fabricados em tecido de poliéster de alta qualidade e durabilidade.
								As artes e logotipos são impressos digitalmente em impressoras HP Látex, tecnologia inovadora de impressão ecologicamente correta,
								com alta resolução, fidelidade de cores e extremamente resistente.</p>

								<h3>RVB, garantia do melhor Inflável!</h3>
								<p>Nossos Infláveis Promocionais são produzidas com materiais de qualidade por profissionais com 20 anos de experiencia,
									que garantem o menor prazo de entrega do mercado. Os Infláveis da RVB tem motor embutido, iluminação em Led com grade de proteção,
									acompanham acessórios como: sacola com identificação, extensão elétrica e kit para pequenos reparos e como opcional estacas de aço.</p>

								<h3>Sobre a montagem e peso dos Infláveis</h3>
								<p>A montagem é bem simples, não é necessário um técnico. O tempo de montagem depende do tamanho e formato,
									mas em média 5 minutos ele esta perfeitamente cheio. O peso do Inflável varia com o tamanho,
									mas como referência uma Tenda Inflável de 3×3 metros pesa 15 quilos.</p>
							</div>

					</div>
				</div>

				<div class="row mt-40">
					<div class="col-12">
						<div class="feature-images__two small-mt__10">
							<div class="modern-grid-image-box row row--30">

								<div class="single-item wow move-up col-lg-4 col-md-6 section-space--mt_60  small-mt__40">
									<!-- ht-box-icon Start -->
									<a href="#" class="ht-box-images style-02">
										<div class="image-box-wrap">
											<div class="box-image">
												<img class="img-fulid" src="assets/images/modelos-inflaveis/tunil-inflavel.png" alt="Tunil inflavel" height="128">
											</div>
											<div class="content">
												<h6 class="heading">TÚNEL INFLÁVEL </h6>
											</div>
										</div>
									</a>
									<!-- ht-box-icon End -->
								</div>

								<div class="single-item wow move-up col-lg-4 col-md-6 section-space--mt_60">
									<!-- ht-box-icon Start -->
									<a href="#" class="ht-box-images style-02">
										<div class="image-box-wrap">
											<div class="box-image">
												<img class="img-fulid" src="assets/images/modelos-inflaveis/tenda-inflavel.png"
													alt="Tenda inflavel" height="128">
											</div>
											<div class="content">
												<h6 class="heading">TENDA INFLÁVEL</h6>
											</div>
										</div>
									</a>
									<!-- ht-box-icon End -->
								</div>

								<div class="single-item wow move-up col-lg-4 col-md-6 section-space--mt_60">
									<!-- ht-box-icon Start -->
									<a href="#" class="ht-box-images style-02">
										<div class="image-box-wrap">
											<div class="box-image">
												<img class="img-fulid" src="assets/images/modelos-inflaveis/balao-promocional.png"
													alt="Balão promocional" height="128">
											</div>
											<div class="content">
												<h6 class="heading">BALÃO PROMOCIONAL</h6>
											</div>
										</div>
									</a>
									<!-- ht-box-icon End -->
								</div>

								<div class="single-item wow move-up col-lg-4 col-md-6 section-space--mt_60">
									<!-- ht-box-icon Start -->
									<a href="#" class="ht-box-images style-02">
										<div class="image-box-wrap">
											<div class="box-image">
												<img class="img-fulid" src="assets/images/modelos-inflaveis/portal-inflavel.png"
													alt="Porta inflavel" height="128">
											</div>
											<div class="content">
												<h6 class="heading">PORTAL INFLÁVEL</h6>
											</div>
										</div>
									</a>
									<!-- ht-box-icon End -->
								</div>

								<div class="single-item wow move-up col-lg-4 col-md-6 section-space--mt_60">
									<!-- ht-box-icon Start -->
									<a href="#" class="ht-box-images style-02">
										<div class="image-box-wrap">
											<div class="box-image">
												<img class="img-fulid" src="assets/images/modelos-inflaveis/painel-inflavel.png"
													alt="Painel inflavel" height="128">
											</div>
											<div class="content">
												<h6 class="heading">PAINEL INFLÁVEL</h6>
											</div>
										</div>
									</a>
									<!-- ht-box-icon End -->
								</div>

								<div class="single-item wow move-up col-lg-4 col-md-6 section-space--mt_60">
									<!-- ht-box-icon Start -->
									<a href="#" class="ht-box-images style-02">
										<div class="image-box-wrap">
											<div class="box-image">
												<img class="img-fulid" src="assets/images/modelos-inflaveis/totem-inflavel.png"
													alt="TOTEM INFLÁVEL"  height="128">
											</div>
											<div class="content">
												<h6 class="heading">TOTEM INFLÁVEL</h6>
											</div>
										</div>
									</a>
									<!-- ht-box-icon End -->
								</div>

								<div class="single-item wow move-up col-lg-4 col-md-6 section-space--mt_60">
									<!-- ht-box-icon Start -->
									<a href="#" class="ht-box-images style-02">
										<div class="image-box-wrap">
											<div class="box-image">
												<img class="img-fulid" src="assets/images/modelos-inflaveis/logomarca-inflavel.png"
													alt="LOGOMARCA INFLÁVEL"  height="128">
											</div>
											<div class="content">
												<h6 class="heading">LOGOMARCA INFLÁVEL</h6>
											</div>
										</div>
									</a>
									<!-- ht-box-icon End -->
								</div>

								<div class="single-item wow move-up col-lg-4 col-md-6 section-space--mt_60">
									<!-- ht-box-icon Start -->
									<a href="#" class="ht-box-images style-02">
										<div class="image-box-wrap">
											<div class="box-image">
												<img class="img-fulid" src="assets/images/modelos-inflaveis/chute-a-gol-inflavel.png"
													alt="CHUTE A GOL INFLÁVEL"  height="128">
											</div>
											<div class="content">
												<h6 class="heading">CHUTE A GOL INFLÁVEL</h6>
											</div>
										</div>
									</a>
									<!-- ht-box-icon End -->
								</div>

								<div class="single-item wow move-up col-lg-4 col-md-6 section-space--mt_60">
									<!-- ht-box-icon Start -->
									<a href="#" class="ht-box-images style-02">
										<div class="image-box-wrap">
											<div class="box-image">
												<img class="img-fulid" src="assets/images/modelos-inflaveis/portal-inflavel.png"
													alt="POTES INFLÁVEIS"  height="128">
											</div>
											<div class="content">
												<h6 class="heading">POTES INFLÁVEIS</h6>
											</div>
										</div>
									</a>
									<!-- ht-box-icon End -->
								</div>

								<div class="single-item wow move-up col-lg-4 col-md-6 section-space--mt_60">
									<!-- ht-box-icon Start -->
									<a href="#" class="ht-box-images style-02">
										<div class="image-box-wrap">
											<div class="box-image">
												<img class="img-fulid" src="assets/images/modelos-inflaveis/garrafa-e-latas-inflaveis.png"
													alt="GARRAFAS E LATAS INFLÁVEIS"  height="128">
											</div>
											<div class="content">
												<h6 class="heading">GARRAFAS E LATAS INFLÁVEIS</h6>
											</div>
										</div>
									</a>
									<!-- ht-box-icon End -->
								</div>

								<div class="single-item wow move-up col-lg-4 col-md-6 section-space--mt_60">
									<!-- ht-box-icon Start -->
									<a href="#" class="ht-box-images style-02">
										<div class="image-box-wrap">
											<div class="box-image">
												<img class="img-fulid" src="assets/images/modelos-inflaveis/mascote-e-bonecos-inflaveis.png"
													alt="MASCOTES E BONECOS INFLÁVEIS"  height="128">
											</div>
											<div class="content">
												<h6 class="heading">MASCOTES E BONECOS INFLÁVEIS</h6>
											</div>
										</div>
									</a>
									<!-- ht-box-icon End -->
								</div>

								<div class="single-item wow move-up col-lg-4 col-md-6 section-space--mt_60">
									<!-- ht-box-icon Start -->
									<a href="#" class="ht-box-images style-02">
										<div class="image-box-wrap">
											<div class="box-image">
												<img class="img-fulid" src="assets/images/modelos-inflaveis/stand-inflavel.png"
													alt="STAND INFLÁVEL"  height="128">
											</div>
											<div class="content">
												<h6 class="heading">STAND INFLÁVEL</h6>
											</div>
										</div>
									</a>
									<!-- ht-box-icon End -->
								</div>

							</div>
						</div>

						<div class="col-lg-12">
							<div class="feature-list-button-box mt-40 text-center">
								<a href="#" class="ht-btn ht-btn-md bg-spotlight">SOLICITE SEU ORÇAMENTO AGORA <i class="far fa-chevron-circle-right"></i></a>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!--===========  feature-images-wrapper  End =============-->



		<!--===========  feature-large-images-wrapper  Start =============-->
		<div class="main_galeria_de_imagens main_inflaveis_promocionais_imagens">
				<div class="feature-large-images-wrapper section-space--ptb_100">
					<div class="container">

						<div class="row">
							<div class="col-lg-12">
								<!-- section-title-wrap Start -->
								<div class="section-title-wrap text-center section-space--mb_30">
									<h6 class="section-sub-title mb-20">GALERIA DE IMAGENS</h6>
									<h3 class="heading">Nossos resultados são a demonstração <br> de toda atenção dedicação.</h3>
								</div>
								<!-- section-title-wrap Start -->
							</div>
						</div>

						<div class="row">
							<div class="col-12">
								<div class="row small-mt__30">

									<div class="col-lg-3 col-md-6 mt-30">
										<!-- Box large image warap Start -->
										<a href="#" class="box-large-image__two">
											<div class="box-large-image__two__box">
												<div class="box-large-image__midea">
													<div class="single-gallery__thum bg-item-images bg-img"
														data-bg="assets/images/box-image/droga-raia.jpg"></div>
												</div>

												<div class="box-info">
													<h5 class="heading">DrogaRaia</h5>
													<!-- <div class="box-more-info">
														<div class="text">We provide the most responsive and functional IT design for companies and
															businesses worldwide.</div>
														<div class="btn">
															<i class="button-icon far fa-long-arrow-right"></i>
														</div>
													</div> -->
												</div>
											</div>
										</a>
										<!-- Box large image warap End -->
									</div>

									<div class="col-lg-6 col-md-6  mt-30">
										<!-- Box large image warap Start -->
										<a href="#" class="box-large-image__two">
											<div class="box-large-image__two__box">
												<div class="box-large-image__midea">
													<div class="single-gallery__thum bg-item-images bg-img"
														data-bg="assets/images/box-image/wizard.jpg"></div>
												</div>

												<div class="box-info">
													<h5 class="heading">Herbalife</h5>
												</div>
											</div>
										</a>
										<!-- Box large image warap End -->
									</div>

									<div class="col-lg-3 col-md-6 mt-30">
										<!-- Box large image warap Start -->
										<a href="#" class="box-large-image__two">
											<div class="box-large-image__two__box">
												<div class="box-large-image__midea">
													<div class="single-gallery__thum bg-item-images bg-img"
														data-bg="assets/images/box-image/herbalife.jpg"></div>
												</div>

												<div class="box-info">
													<h5 class="heading">Herbalife</h5>
												</div>
											</div>
										</a>
										<!-- Box large image warap End -->
									</div>

									<div class="col-lg-4 col-md-6 mt-30">
										<!-- Box large image warap Start -->
										<a href="#" class="box-large-image__two">
											<div class="box-large-image__two__box">
												<div class="box-large-image__midea">
													<div class="single-gallery__thum bg-item-images bg-img"
														data-bg="assets/images/box-image/now.jpg"></div>
												</div>

												<div class="box-info">
													<h5 class="heading">Net Now</h5>
												</div>
											</div>
										</a>
										<!-- Box large image warap End -->
									</div>

									<div class="col-lg-4 col-md-6 mt-30">
										<!-- Box large image warap Start -->
										<a href="#" class="box-large-image__two">
											<div class="box-large-image__two__box">
												<div class="box-large-image__midea">
													<div class="single-gallery__thum bg-item-images bg-img"
														data-bg="assets/images/box-image/ciesp.jpg"></div>
												</div>

												<div class="box-info">
													<h5 class="heading">Ciesp</h5>
												</div>
											</div>
										</a>
										<!-- Box large image warap End -->
									</div>

									<div class="col-lg-4 col-md-6 mt-30">
										<!-- Box large image warap Start -->
										<a href="#" class="box-large-image__two">
											<div class="box-large-image__two__box">
												<div class="box-large-image__midea">
													<div class="single-gallery__thum bg-item-images bg-img"
														data-bg="assets/images/box-image/fiat.jpg"></div>
												</div>

												<div class="box-info">
													<h5 class="heading">Fiat</h5>
												</div>
											</div>
										</a>
										<!-- Box large image warap End -->
									</div>

								</div>
								<div class="col-lg-12">
									<div class="feature-list-button-box mt-40 text-center">
										<a href="#" class="ht-btn ht-btn-md">Veja Mais</a>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
			<!--===========  feature-large-images-wrapper  End =============-->

			<!--===========  Vantagens =============-->
			<div class="main_vantangens">
				<div class="container">

					<h2>Vantagens dos Infláveis Promocionais</h2>
					<h4>Com o passar dos anos, os infláveis têm provado, continuamente, ser uma ferramenta de marketing altamente efetiva, pelo simples fato de que poucos produtos têm a oferecer <br> um impacto visual tão alto com uma ótima facilidade de uso e um excelente custo-benefício.</h4>

					<div class="row mt-40 mb-40">
						<div class="col-md-12 col-lg-6">
							<ul>
								<li><i class="far fa-check-circle"></i> Simples de montar e desmontar, comparado com estruturas rígidas;</li>
								<li><i class="far fa-check-circle"></i> Altamente customizáveis em formato, tamanho, cor e impressão digital;</li>
								<li><i class="far fa-check-circle"></i> Infláveis de alta qualidade tem uma longa durabilidade e são de fácil manutenção;</li>
								<li><i class="far fa-check-circle"></i> Infláveis de propaganda são fabricados com materiais, equipamentos e metodologia da mais alta qualidade;</li>
								<li><i class="far fa-check-circle"></i> Rápido de montar e desmontar, economizando tempo e trabalho;</li>
								<li><i class="far fa-check-circle"></i> Melhor reconhecimento do seu produto e da sua marca;</li>
								<li><i class="far fa-check-circle"></i> Oportunidades de patrocínio de alto impacto e alta visibilidade;</li>
								<li><i class="far fa-check-circle"></i> Você pode mudar os banners, mudando completamente a mensagem da sua propaganda (em alguns casos).</li>
							</ul>
						</div>
						<div class="col-md-12 col-lg-6">
							<ul>
								<li><i class="far fa-check-circle"></i> Quando vazio, se torna um pacote compacto, que representa aproximadamente 10% do seu tamanho inflado, excelente para transportar e armazenar;</li>
								<li><i class="far fa-check-circle"></i> Podem ser utilizados em ambientes abertos ou fechados, dia ou noite;</li>
								<li><i class="far fa-check-circle"></i> Flexibilidade para usar em vários e diferentes tipos de eventos;</li>
								<li><i class="far fa-check-circle"></i> Baixo consumo de energia;</li>
								<li><i class="far fa-check-circle"></i> Baixo custo de operação;</li>
								<li><i class="far fa-check-circle"></i> Aumento nas vendas no seu PDV;</li>
								<li><i class="far fa-check-circle"></i> Iluminação interna (em alguns casos);</li>
							</ul>
						</div>
					</div>

				</div>
			</div>
			<!--===========  Vantagens  End =============-->

			<!--===========  Um pouco mais sobre inflaveis promocionais =============-->
			<div class="main_about_inflaveis_promocionais bg-gray">
				<div class="container">
					<div class="col-md-12">
						<h2 class="mt-40 mb-40">Um pouco mais sobre os Infláveis Promocionais</h2>
						<p class="text-center">
							Os Infláveis Promocionais são uma ferramenta de marketing altamente efetiva. Totalmente customizáveis em formato,
							tamanho, cores e impressão digital. Somos referência mundial na fabricação de balões de ar quente tripulados com formatos especiais.
							E foi através dessa expertise que desenvolvemos, com qualidade premium,
							a produção de Balões e Infláveis em diversos formatos e tamanho no Brasil. Se você busca soluções de marketing com infláveis promocionais,
							conte com a RVB. Deixe seu contato e, como a nossa experiência, ajudaremos você a desenvolver o seu projeto da melhor forma,
							focando sempre na eficiência e qualidade.
						</p>

						<div class="feature-list-button-box mt-40 mb-40 text-center">
							<a href="#" class="ht-btn ht-btn-md bg-spotlight">SOLICITE SEU ORÇAMENTO AGORA <i class="far fa-chevron-circle-right"></i></a>
						</div>
					</div>
				</div>
			</div>
			<!--===========  Um pouco mais sobre inflaveis promocionais  End =============-->

			<!--===========  Perguntas frequentes =============-->
			<div class="accordion-wrapper section-space--ptb_100">
			<div class="container">

				<div class="row">
					<div class="col-lg-12">
						<!-- section-title-wrap Start -->
						<div class="section-title-wrap text-center section-space--mb_20">
							<h3 class="heading">Perguntas mais frequentes</h3>
						</div>
						<!-- section-title-wrap Start -->
					</div>
				</div>

				<div class="row">
					<div class="col-lg-6">
						<div class="faq-wrapper section-space--mt_40">
							<div id="accordion">


								<div class="card">
									<div class="card-header">
										<h5 class="mb-0">
											<button class="btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false">
											O motor precisa ficar ligado? <span><i class="far fa-chevron-circle-down"></i>
													<i class="far fa-chevron-circle-up"></i> </span>
											</button>
										</h5>
									</div>
									<div id="collapseTwo" class="collapse" data-parent="#accordion" style="">
										<div class="card-body">
											<p>Sim. O Inflável é fabricado em tecido e costurado, logo o motor precisa ficar ligado o tempo todo.</p>
										</div>
									</div>
								</div>


								<div class="card">
									<div class="card-header">
										<h5 class="mb-0">
											<button class="btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false">
											Como funciona a garantia do inflável? <span> <i class="far fa-chevron-circle-down"></i>
													<i class="far fa-chevron-circle-up"></i> </span>
											</button>
										</h5>
									</div>
									<div id="collapseThree" class="collapse" data-parent="#accordion">
										<div class="card-body">
											<p>We reduce redundant complex calculations and lengthy erroneous code texts with simpler ones to
												ensure Mitech would run seamlessly and the design is reserved in its best form when viewed from
												a wide range of mobile devices &amp; browsers. </p>
										</div>
									</div>
								</div>

								<div class="card">
									<div class="card-header">
										<h5 class="mb-0">
											<button class="collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false">
											Quanto tempo dura o inflável? <span> <i class="far fa-chevron-circle-down"></i>
													<i class="far fa-chevron-circle-up"></i> </span>
											</button>
										</h5>
									</div>
									<div id="collapseFour" class="collapse" data-parent="#accordion">
										<div class="card-body">
											<p>Our service offerings to enhance customer experience throughout the product lifecycle includes
												– test and repair, service management, and end-to-end warranty management. </p>
										</div>
									</div>
								</div>

							</div>

						</div>
					</div>

					<div class="col-lg-6">
						<!-- Start toggles -->
						<div class="toggles-wrapper section-space--mt_40">
							<div class="faq-wrapper">
								<div id="faq-toggles">
									<div class="card">
										<div class="card-header">
											<h5 class="mb-0">
												<button class="btn-link collapsed" data-toggle="collapse" data-target="#faq_two" aria-expanded="false">
												Quanto tempo dura o inflável?<span> <i class="far fa-chevron-circle-down"></i>
														<i class="far fa-chevron-circle-up"></i> </span>
												</button>
											</h5>
										</div>
										<div id="faq_two" class="collapse">
											<div class="card-body">
												<p>Mitech takes into consideration every little detail to make sure the system run smoothly and
													responsively. Mitech employs a new technique called Minified Technology for securing
													customers' database &amp; building up highly confidential firewalls. </p>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header">
											<h5 class="mb-0">
												<button class="btn-link collapsed" data-toggle="collapse" data-target="#faq_three" aria-expanded="false">
													Como funciona a garantia do motor? <span> <i class="far fa-chevron-circle-down"></i>
														<i class="far fa-chevron-circle-up"></i> </span>
												</button>
											</h5>
										</div>
										<div id="faq_three" class="collapse">
											<div class="card-body">
												<p>We reduce redundant complex calculations and lengthy erroneous code texts with simpler ones
													to ensure Mitech would run seamlessly and the design is reserved in its best form when viewed
													from a wide range of mobile devices &amp; browsers. </p>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
						<!-- End toggles -->
					</div>
				</div>
			</div>
		</div>
			<!--===========  Perguntas frequentes  End =============-->

	</div>



	<?php include_once('partials/balonismo.php'); ?>
	<?php include('partials/newsletter.php'); ?>
	<?php include_once('footer.php'); ?>
