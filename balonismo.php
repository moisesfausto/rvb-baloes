<?php require_once('header.php'); ?>
<?php include_once('partials/diferenciais.php'); ?>

<!-- breadcrumb-area start -->
<div class="breadcrumb-area main_ts_balonismo_bg">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="breadcrumb_box text-center">
					<h2 class="breadcrumb-title">Balonismo</h2>
					<!-- breadcrumb-list start -->
					<ul class="breadcrumb-list">
						<li class="breadcrumb-item"><a href="index.html">Home</a></li>
						<li class="breadcrumb-item active">Balonismo</li>
					</ul>
					<!-- breadcrumb-list end -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- breadcrumb-area end -->

<div id="main-wrapper">
	<div class="site-wrapper-reveal">


		<div class="tabs-wrapper main_ts_balonismo section-space--ptb_100">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="section-title-wrapper text-center section-space--mb_60 wow move-up animated"
							style="visibility: visible;">
							<h3 class="section-title"><span class="text-color-spotlight">Calendário de Eventos de Balonismo</span>
							</h3>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 ht-tab-wrap">
						<div class="row">
							<div class="col-12 text-center wow move-up  animated" style="visibility: visible;">
								<ul class="nav justify-content-center ht-tab-menu" role="tablist">
									<li class="tab__item nav-item active">
										<a class="nav-link active" id="nav-tab1" data-toggle="tab" href="#calendario-nacional" role="tab"
											aria-selected="true">CALENDÁRIO NACIONAL</a>
									</li>
									<li class="tab__item nav-item">
										<a class="nav-link" id="nav-tab2" data-toggle="tab" href="#calendario-internacional" role="tab"
											aria-selected="false">CALENDÁRIO INTERNACIONAL</a>
									</li>
								</ul>
							</div>
						</div>

						<div class="tab-content ht-tab__content wow move-up animated" style="visibility: visible;">

							<div class="tab-pane fade show active" id="calendario-nacional" role="tabpanel">
								<div class="tab-history-wrap section-space--mt_60">
									<div class="row">
										<div class="col-lg-12">
											<div class="ht-simple-job-listing move-up animate">
												<div clas="list">
													<div class="item">
														<div class="row">
															<div class="col-md-3">
																<div class="job-info">
																	<h5 class="job-name">1º Festival Internacional de Balonismo de São Miguel das Missões
																	</h5>
																	<span class="job-time">20 de Janeiro de 2021</span>
																</div>
															</div>

															<div class="col-md-6">
																<div class="job-description">No último fim de semana foi impossível ignorar que havia
																	algo diferente na paisagem de Resende. O município recebeu o Festival de Balonismo e
																	enormes balões coloridos, reunindo balonistas e paraquedistas para um verdadeiro
																	espetáculo. No último fim de semana foi impossívelignorar que havia algo diferente na
																	paisagem de Resende.</div>
															</div>

															<div
																class="col-md-3 d-flex align-items-center justify-content-center justify-content-lg-end">
																<div class="job-button text-md-center">
																	<a class="ht-btn ht-btn-md ht-btn--solid bg-spotlight" href="#">
																		<span class="button-text">Veja Mais</span>
																	</a>
																</div>
															</div>
														</div>
													</div>
													<div class="item">
														<div class="row">
															<div class="col-md-3">
																<div class="job-info">
																	<h5 class="job-name">1º Festival Internacional de Balonismo de São Miguel das Missões
																	</h5>
																	<span class="job-time">20 de Janeiro de 2021</span>
																</div>
															</div>

															<div class="col-md-6">
																<div class="job-description">No último fim de semana foi impossível ignorar que havia
																	algo diferente na paisagem de Resende. O município recebeu o Festival de Balonismo e
																	enormes balões coloridos, reunindo balonistas e paraquedistas para um verdadeiro
																	espetáculo. No último fim de semana foi impossívelignorar que havia algo diferente na
																	paisagem de Resende.</div>
															</div>

															<div
																class="col-md-3 d-flex align-items-center justify-content-center justify-content-lg-end">
																<div class="job-button text-md-center">
																	<a class="ht-btn ht-btn-md ht-btn--solid bg-spotlight" href="#">
																		<span class="button-text">Veja Mais</span>
																	</a>
																</div>
															</div>
														</div>
													</div>
													<div class="item">
														<div class="row">
															<div class="col-md-3">
																<div class="job-info">
																	<h5 class="job-name">1º Festival Internacional de Balonismo de São Miguel das Missões
																	</h5>
																	<span class="job-time">20 de Janeiro de 2021</span>
																</div>
															</div>

															<div class="col-md-6">
																<div class="job-description">No último fim de semana foi impossível ignorar que havia
																	algo diferente na paisagem de Resende. O município recebeu o Festival de Balonismo e
																	enormes balões coloridos, reunindo balonistas e paraquedistas para um verdadeiro
																	espetáculo. No último fim de semana foi impossívelignorar que havia algo diferente na
																	paisagem de Resende.</div>
															</div>

															<div
																class="col-md-3 d-flex align-items-center justify-content-center justify-content-lg-end">
																<div class="job-button text-md-center">
																	<a class="ht-btn ht-btn-md ht-btn--solid bg-spotlight" href="#">
																		<span class="button-text">Veja Mais</span>
																	</a>
																</div>
															</div>
														</div>
													</div>
													<div class="item">
														<div class="row">
															<div class="col-md-3">
																<div class="job-info">
																	<h5 class="job-name">1º Festival Internacional de Balonismo de São Miguel das Missões
																	</h5>
																	<span class="job-time">20 de Janeiro de 2021</span>
																</div>
															</div>

															<div class="col-md-6">
																<div class="job-description">No último fim de semana foi impossível ignorar que havia
																	algo diferente na paisagem de Resende. O município recebeu o Festival de Balonismo e
																	enormes balões coloridos, reunindo balonistas e paraquedistas para um verdadeiro
																	espetáculo. No último fim de semana foi impossívelignorar que havia algo diferente na
																	paisagem de Resende.</div>
															</div>

															<div
																class="col-md-3 d-flex align-items-center justify-content-center justify-content-lg-end">
																<div class="job-button text-md-center">
																	<a class="ht-btn ht-btn-md ht-btn--solid bg-spotlight" href="#">
																		<span class="button-text">Veja Mais</span>
																	</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="tab-pane fade" id="calendario-internacional" role="tabpanel">
								<div class="tab-history-wrap section-space--mt_60">
									<div class="row">
										<div class="col-lg-6 ">

										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>


		<div class="main_paixao_balonismo">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2>Uma Paixão chamada Balonismo</h2>
						<h4>É um esporte aeronáutico praticado com um Balão de Ar Quente e inúmeras pessoas de toda parte do mundo
							praticam esse hobby. Os campeonatos de balonismo são elaborados de forma bastante criteriosa e as
							“performances” levam em consideração a precisão dos pilotos em voo. Aproveite para conhecer nossa história
							no Balonismo!</h4>
					</div>


					<div class="col-md-12 col-lg-6" style="margin-bottom: 90px;">
						<div class="cover_img"><img src="assets/images/balonismo/FOTO-BALONISMO-01.jpg" class="img-fluid"
								alt="Aventura e Balões de Ar Quente caminham juntos!"></div>
					</div>
					<div class="col-md-12 col-lg-6" style="margin-bottom: 90px;">
						<div class="descricao">
							<h6>Aventura e Balões de Ar Quente caminham juntos!</h6>
							<p>Com Balões de Ar Quente, pode ser realizado um voo livre sobre um determinado local ou o voo cativo, em
								que o balão fica preso por cordas possibilitando as pessoas terem contato direto com o balão. Pilotos
								mais experientes e com espírito aventureiro geralmente se envolvem em voos mais audaciosos, como vencer
								grandes altitudes, sobrevoar parques nacionais, travessias de um continente para uma ilha, ou mesmo dar
								a volta ao mundo a bordo de um balão. O balão alcança grandes altitudes e atravessa grandes distâncias
								na mais perfeita segurança.</p>

							<p>Segundo estatísticas da Divisão Aero-desportiva do Departamento de
								Aeronáutica Civil e da Associação Brasileira de Balonismo, o balão é a mais segura das aeronaves, com
								índice de acidentes praticamente nulo. É considerado, segundo a Federação Aeronáutica Internacional, o
								esporte aéreo mais seguro do mundo há mais de 20 anos, sendo por isso, a única aeronave que não exige o
								uso de pára-quedas.</p>

							<p>Você se interessa por Balonismo? Que tal conhecer nossos projetos?</p>
						</div>
					</div>

					<div class="col-md-12 col-lg-6">
						<div class="descricao">
							<h6>Construção de balões <br> no estilo RVB</h6>
							<p>Voar é tudo de bom. Ainda mais de balão! Nós aqui da RVB Balões amamos o que fazemos. Podemos elaborar
								o projeto do balão dos seus sonhos e transformá-lo em realidade. Tudo isso com materiais de qualidade,
								projetos tecnicamente perfeitos e uma equipe de profissionais, com mais de 20 anos de excelência.</p>

							<p>Realizamos também projetos especiais para as empresas que gostariam de contar com essa excelente
								ferramenta de marketing.</p>

							<p>Entre em contato com a gente para saber mais detalhes.</p>

							<div class="mt-40 mb-4 mb-lg-0">
								<a href="#" class="ht-btn ht-btn-md bg-spotlight">SOLICITE SEU ORÇAMENTO AGORA <i
										class="far fa-chevron-circle-right ml-2"></i></a>
							</div>
						</div>
					</div>
					<div class="col-md-12 col-lg-6">
						<div class="cover_img"><img src="assets/images/balonismo/FOTO-BALONISMO-02.jpg" class="img-fluid"
								alt="Construção de balões no estilo RVB"></div>
					</div>


				</div>
			</div>
		</div>


		<div class="main_ts_baloes_convencionais bg-gray">
			<div class="container">

				<h2>Balões Convencionais</h2>
				<h4>O Balão Convencional, nada mais é aquele com o formato parecido com uma gota, muito usado para voos
					livres, ações promocionais e competições.</h4>

				<div class="row justify-content-center">
					<div class="col-lg-4"><img src="assets/images/balonismo/balao-convencional-01.jpg" alt="Balão Convencional"
							class="img-fluid"></div>
					<div class="col-lg-4"><img src="assets/images/balonismo/balao-convencional-02.jpg" alt="Balão Convencional"
							class="img-fluid"></div>
					<div class="col-lg-4"><img src="assets/images/balonismo/balao-convencional-01.jpg" alt="Balão Convencional"
							class="img-fluid"></div>

					<div class="mt-40 mb-4 mb-lg-0">
						<a href="#" class="ht-btn ht-btn-md bg-spotlight">SOLICITE SEU ORÇAMENTO AGORA <i
								class="far fa-chevron-circle-right ml-2"></i></a>
					</div>
				</div>
			</div>
		</div>

		<div class="main_ts_baloes_special_shape">
			<div class="container">

				<h2>Special Shape</h2>
				<h4>Você já deve ter visto na mídia algum balão de formato especial. Geralmente são aeronaves em forma de algum
					formato específico, podendo ser um animal, desenho ou até mesmo um produto. A RVB Balões é uma das maiores
					fabricantes do mundo deste tipo de balão e podemos dar asas à sua imaginação. Faça um orçamento sem
					compromisso conosco. Podemos garantir que não vai se arrepender!</h4>

				<div class="row justify-content-center">
					<div class="col-lg-4"><img src="assets/images/balonismo/special-shape-01.jpg" alt="Special Shape"
							class="img-fluid"></div>
					<div class="col-lg-4"><img src="assets/images/balonismo/special-shape-02.jpg" alt="Special Shape"
							class="img-fluid"></div>
					<div class="col-lg-4"><img src="assets/images/balonismo/special-shape-03.jpg" alt="Special Shape"
							class="img-fluid"></div>

					<div class="mt-40 mb-4 mb-lg-0">
						<a href="#" class="ht-btn ht-btn-md">SOLICITE SEU ORÇAMENTO AGORA <i
								class="far fa-chevron-circle-right ml-2"></i></a>
					</div>
				</div>
			</div>
		</div>

		<div class="main_balonismo_profissional">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h3>Balonismo Promocional</h3>
						<h6>É impossível ficar insensível ao ver um balão gigante, voando livre ou preso a cordas, em eventos ou
							locais de grande circulação de pessoas. É uma grande sacada ter sua marca ou produto impresso num balão,
							pois ela ficará para sempre na memória do seu público alvo! O Balonismo pode, inclusive, ser utilizado
							como mídia espontânea, para que você tenha mais visibilidade nas mídias sociais.</h6>
						<h4>Os principais tipos de ações de Balonismo Promocional são:</h4>
					</div>
				</div>
				<div class="row justify-content-between" style="margin-top: 72px;">
					<div class="col-md-12 col-lg-5">
						<div class="main_types_actions">
							<img src="assets/images/balonismo/FOTO-BALONISMO-01.jpg" class="img-fluid"
								alt="Ação Promocional em Voo Livre:">
							<h3>Ação Promocional em Voo Livre:</h3>
							<p>Trata-se de sobrevoar cidades ou locais de grande circulação, para divulgação de marca ou produto. No
								voo é possível levar clientes ou convidados, para um passeio de balão.</p>
							<p>O piloto pode mudar a altura do voo e sobrevoar com tranquilidade a cidade e região do evento.</p>
							<div class="mt-40 mb-4 mb-lg-0">
								<a href="#" class="ht-btn ht-btn-md bg-spotlight">SOLICITE SEU ORÇAMENTO AGORA <i
										class="far fa-chevron-circle-right ml-2"></i></a>
							</div>
						</div>
					</div>
					<div class="col-md-12 col-lg-5">
						<div class="main_types_actions">
							<h3>Ação Promocional em Voo Cativo:</h3>
							<p>Consiste em manter o balão inflado e preso com cordas no local doevento, para divulgar e promover de
								forma diferenciada, além disso, sinaliza o evento e serve de entretenimento para as pessoas.</p>
							<p>O voo possibilita desfrutar por alguns minutos da paisagem e sentir a experiência de voar.</p>
							<div class="mt-40 mb-4 mb-lg-0">
								<a href="#" class="ht-btn ht-btn-md bg-spotlight">SOLICITE SEU ORÇAMENTO AGORA <i
										class="far fa-chevron-circle-right ml-2"></i></a>
							</div>
							<img src="assets/images/balonismo/FOTO-BALONISMO-01.jpg" class="img-fluid"
								alt="Ação Promocional em Voo Cativo:" style="margin-top: 140px;">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="main_types_actions text-center">
							<p>Com o Balonismo Promocional, a sua marca é trabalhada de maneira menos incisiva. É uma forma de ação
								promocional que proporciona um relacionamento mais amigável com seus clientes. Nenhum jornalista recusa
								um convite para um voo de balão, sabe por que? Porque ele vai encantar seu público ao escrever sobre
								essa
								experiência única.</p>

							<p>Na prestação do serviço, a RVB Balões, com total autoridade no ramo, proporciona a
								experiência do voo de balão com total segurança. Além disso, todos os nossos balões são sempre operados
								por pilotos habilitados junto ao ANAC. Com visual impactante, este tipo de ação promocional busca fixar
								na memória das pessoas, o balão com sua marca aplicada, por exemplo: <strong>“Olha mamãe, o balão da
									Caixa
									Econômica, que lindo!”</strong></p>

							<p>Esse é nosso objetivo em uma ação promocional: encantar, divertir, fidelizar as pessoas e
								trazer resultados e satisfação para nossos clientes.</p>

							<p>A RVB Balões, possui uma experiência em mais 200
								ações promocionais, com aproximadamente 1000 horas de voos promocionais, por todo Brasil, em eventos
								como
								campanhas publicitárias, shows, rodeios, festas comemorativas, filmagens, casamentos, entretenimento em
								hotéis, divulgação etc. Cuidamos de toda a infraestrutura necessária, incluindo projeto, construção,
								operação e manutenção dos balões de ar quente, trâmites junto à aeronáutica.</p>

							<p>Consulte hoje mesmo a RVB Balões, será uma satisfação poder atendê-lo!</p>
						</div>
						<div class="mt-40 mb-4 mb-lg-0 text-center">
							<a href="#" class="ht-btn ht-btn-md bg-spotlight">SOLICITE SEU ORÇAMENTO AGORA <i
									class="far fa-chevron-circle-right ml-2"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="main_ts_baloes_convencionais main_competicao_balonismo bg-gray">
			<div class="container">

				<h2>Competições de Balonismo</h2>
				<h4>A RVB Balões participa das principais competições de balonismo a nível nacional e internacional. Para este
					ano nossa equipe já está garantida no campeonato brasileiro da modalidade, em Boituva. Também participaremos
					de festivais no exterior, entre eles o de Albuquerque/EUA (maior evento de balonismo do mundo) e León, no
					México. Aqui no Brasil participamos da maioria dos eventos que são realizados. Podemos elevar sua marca no
					lugar mais alto. Entre em contato conosco para mais informações.</h4>
				<h3>Organização de Eventos</h3>
				<h4>Quer organizar um festival de balonismo na sua cidade e não sabe como? A RVB Balões e Infláveis pode te
					ajudar. Entre em contato conosco para solicitar os detalhes técnicos e verificar a possibilidade de realização
					de um evento no seu município.</h4>
				<h3>Campeonatos</h3>

				<div class="row justify-content-center">
					<div class="col-lg-4">
						<div class="wrap_competicao">
							<a href="">
								<img src="assets/images/balonismo/balao-convencional-01.jpg" alt="Balão Convencional" class="img-fluid">
								<h3>31º Campeonato Brasileiro de Balonismo em Casa Branca</h3>
							</a>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="wrap_competicao">
							<a href="">
								<img src="assets/images/balonismo/balao-convencional-02.jpg" alt="Balão Convencional" class="img-fluid">
								<h3>31º Campeonato Brasileiro de Balonismo em Casa Branca</h3>
							</a>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="wrap_competicao">
							<a href="">
								<img src="assets/images/balonismo/balao-convencional-01.jpg" alt="Balão Convencional" class="img-fluid">
								<h3>31º Campeonato Brasileiro de Balonismo em Casa Branca</h3>
							</a>
						</div>
					</div>

					<div class="col-lg-4">
						<div class="wrap_competicao">
							<a href="">
								<img src="assets/images/balonismo/balao-convencional-01.jpg" alt="Balão Convencional" class="img-fluid">
								<h3>31º Campeonato Brasileiro de Balonismo em Casa Branca</h3>
							</a>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="wrap_competicao">
							<a href="">
								<img src="assets/images/balonismo/balao-convencional-02.jpg" alt="Balão Convencional" class="img-fluid">
								<h3>31º Campeonato Brasileiro de Balonismo em Casa Branca</h3>
							</a>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="wrap_competicao">
							<a href="">
								<img src="assets/images/balonismo/balao-convencional-01.jpg" alt="Balão Convencional" class="img-fluid">
								<h3>31º Campeonato Brasileiro de Balonismo em Casa Branca</h3>
							</a>
						</div>
					</div>


				</div>
			</div>



			<div class="ht-pagination mt-30 pagination justify-content-center">
				<div class="pagination-wrapper">
					<ul class="page-pagination">
						<li><a class="prev page-numbers" href="#">ANTERIOR</a></li>
						<li><a class="page-numbers current" href="#">1</a></li>
						<li><a class="page-numbers" href="#">2</a></li>
						<li><a class="next page-numbers" href="#">PRÓXIMO</a></li>
					</ul>
				</div>
			</div>
		</div>

	</div>


	<?php include('partials/newsletter.php'); ?>
	<?php include_once('footer.php'); ?>
