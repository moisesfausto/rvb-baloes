## Configurações básicas do wp_config.php

define('DB_NAME', 'nome_do_banco_de_dados_aqui');

define('DB_USER', 'nome_de_usuario_aqui');

define('DB_PASSWORD', 'senha_aqui');

define('DB_HOST', 'localhost');

define('DB_CHARSET', 'utf8');

define('DB_COLLATE', '');

define('AUTH_KEY',         'coloque a sua frase única aqui');
define('SECURE_AUTH_KEY',  'coloque a sua frase única aqui');
define('LOGGED_IN_KEY',    'coloque a sua frase única aqui');
define('NONCE_KEY',        'coloque a sua frase única aqui');
define('AUTH_SALT',        'coloque a sua frase única aqui');
define('SECURE_AUTH_SALT', 'coloque a sua frase única aqui');
define('LOGGED_IN_SALT',   'coloque a sua frase única aqui');
define('NONCE_SALT',       'coloque a sua frase única aqui');

$table_prefix = 'wp_';

### servidor
define('WP_DEBUG', false);

### localhost
define('WP_DEBUG', true);
