<?php include_once('header.php'); ?>
<?php include_once('partials/diferenciais.php'); ?>

<!-- breadcrumb-area start -->
<div class="breadcrumb-area main_ts_our_history">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="breadcrumb_box text-center">
					<h2 class="breadcrumb-title">RVB Balões</h2>
					<!-- breadcrumb-list start -->
					<ul class="breadcrumb-list">
						<li class="breadcrumb-item"><a href="index.html">Home</a></li>
						<li class="breadcrumb-item active">RBV BALÕES</li>
					</ul>
					<!-- breadcrumb-list end -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- breadcrumb-area end -->



<div id="main-wrapper">
	<div class="site-wrapper-reveal">
		<!--===========  our history wrapper  Start =============-->
		<div class="our-history-wrapper section-space--ptb_100">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<!-- section-title-wrap Start -->
						<div class="section-title-wrap text-center section-space--mb_30">
							<h3 class="heading mb-20">Eu sou o <span class="text-color-spotlight">Luis Silvestre</span> e vou
								compartilhar um <br> pouco da minha história com vocês.</h3>
							<p class="title-dec-text">Em 1988 minha trajetória profissional se iniciou e com a ajuda de minha mãe,
								montamos, em casa, uma pequena confecção de uniformes escolares, camisetas promocionais e começamos a
								vender em casa mesmo, mas logo construímos uma loja, que virou papelaria e em pouco tempo toda família
								estava envolvida e já tínhamos 4 lojas e uma confecção e estamparia, com mais de 500 m².</p>
						</div>
						<!-- section-title-wrap Start -->
					</div>
				</div>

				<div class="timeline-area section-space--pt_60">
					<div class="row">
						<div class="col-lg-12">
							<div class="ht-timeline style-01">
								<ul class="tm-timeline-list ht-animation-queue">
									<li class="line"></li>
									<li class="item animate">
										<div class="dots">
											<div class="middle-dot"></div>
										</div>

										<div class="row">
											<div class="col-md-6 timeline-col left timeline-feature">
												<div class="inner">
													<div class="date-wrap">
														<h2 class="year">1996</h2>
													</div>

													<div class="photo">
														<img src="assets/images/timeline/timeline-image-01.jpg" alt="timeline-image-01"
															class="img-fluid">
													</div>
												</div>
											</div>
											<div class="col-md-6 timeline-col right timeline-info">
												<div class="inner">
													<div class="content-wrap">
														<div class="content-body">
															<h6 class="heading">As festas juninas com fogueira, balão e pipoca sempre foram minhas preferidas, e foi o balão que mudou minha vida.</h6>

															<div class="text">
															Em junho de 1998, passeando em Campos do Jordão, um balão de ar quente gigante me chamou atenção e ele fazia uma ação promocional. A partir daquele momento, percebi que poderia voar de balão e ainda ganhar por isso, a ideia era simplesmente perfeita. Desde então, “entrei de cabeça” no Balonismo. Para adquirir experiência, trabalhei em ações promocionais e participei de campeonatos como equipe para alguns pilotos.</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</li>
									<li class="item animate">
										<div class="dots">
											<div class="middle-dot"></div>
										</div>

										<div class="row">
											<div class="col-md-6 timeline-col left timeline-feature">
												<div class="inner">
													<div class="date-wrap">
														<h6 class="year"> 2000 </h6>
													</div>

													<div class="photo">
														<img src="assets/images/timeline/timeline-image-02.jpg" alt="timeline-image-01"
															class="img-fluid">
													</div>
												</div>
											</div>
											<div class="col-md-6 timeline-col right timeline-info">
												<div class="inner">
													<div class="content-wrap">
														<div class="content-body">
															<h6 class="heading">Entrar para o Balonismo não foi uma tarefa fácil, mas no ano 2000, como o mundo não acabou, finalmente conquistei meu brevê e a liberdade para voar.</h6>

															<div class="text">
																No mesmo ano, paralelamente com a confecção, eu e um sócio fundamos a “Rota dos Ventos Balonismo”, hoje RVB. Começamos como prestadores de serviços e participando de festival e campeonatos pelo Brasil. O foco da empresa era ação promocional, ou seja, usando o balão como meio de publicidade em rodeios, shows, lançamento imobiliário, campanhas publicitárias e outros.
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</li>
									<li class="item animate">
										<div class="dots">
											<div class="middle-dot"></div>
										</div>

										<div class="row">
											<div class="col-md-6 timeline-col left timeline-feature">
												<div class="inner">
													<div class="date-wrap">
														<h6 class="year"> 2002 </h6>
													</div>

													<div class="photo">
														<img src="assets/images/timeline/blog-07-500x350.jpg" alt="timeline-image-01"
															class="img-fluid">
													</div>
												</div>
											</div>
											<div class="col-md-6 timeline-col right timeline-info">
												<div class="inner">
													<div class="content-wrap">
														<div class="content-body">
															<h6 class="heading">Com o aumento da demanda de ações com o balão, deixei as lojas e a confecção com minha família.</h6>

															<div class="text">
																Muitas coisas aconteceram em 2006. Mudamos o nome da empresa para RVB Balões e conquistei meu primeiro Vice-campeonato Brasileiro de Balonismo. Passei por uma fase delicada, porém com a ajuda de meus familiares, amigos e por mérito próprio, conquistamos como cliente a Caixa Econômica Federal, então a empresa retoma o crescimento com força total.
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</li>
									<li class="item animate">
										<div class="dots">
											<div class="middle-dot"></div>
										</div>

										<div class="row">
											<div class="col-md-6 timeline-col left timeline-feature">
												<div class="inner">
													<div class="date-wrap">
														<h6 class="year"> 2007 </h6>
													</div>

													<div class="photo">
														<img src="assets/images/timeline/timeline-image-02.jpg" alt="timeline-image-01"
															class="img-fluid">
													</div>
												</div>
											</div>
											<div class="col-md-6 timeline-col right timeline-info">
												<div class="inner">
													<div class="content-wrap">
														<div class="content-body">
															<h6 class="heading">Em 2007 eu, Luis Silvestre e Marcos Bonimcontro, iniciarmos uma parceria de sucesso que permanece até hoje.</h6>

															<div class="text">
																Com minha experiência em administração e em confecção a RVB (literalmente) decolou e começou a construir Balões de Ar Quente e Infláveis Promocionais. Hoje eu, Luis Silvestre, sou Tetracampeão Brasileiro de Balonismo, e os balões da RVB BALÕES E INFLÁVEIS LTDA voam munda afora, com uma equipe que possui mais de 20 anos de experiência, produzindo infláveis que são retorno garantido para nossos clientes. Continuamente investimos em novas tecnologias e equipamentos, desenvolvemos projetos personalizados, sempre construindo Balões e Infláveis de alta qualidade no mercado.
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		<!--===========  feature-large-images-wrapper  End =============-->

		<div class="main_festivais bg-theme-four">
			<div class="container d-flex flex-column">
				<h2 class="">Festivais</h2>

				<div class="festivais_lugares">
					<span><i class="fas fa-check"></i> Albuquerque Balloon Fiesta desde 2009</span>
					<span><i class="fas fa-check"></i> Festival Internacional Del Globo - Leon México 2015</span>
				</div>

			</div>
		</div>

		<div class="main_campeonatos">
			<div class="container d-flex flex-column">
				<h2 class="">Campeonatos</h2>

				<div class="campeonatos_lugares">
					<ul>
						<li><span><i class="fas fa-check"></i> Campeonato Mundial de Balonismo Áustria 2008</span></li>
						<li><span><i class="fas fa-check"></i> Campeonato Mundial de Balonismo Hungria 2010</span></li>
						<li><span><i class="fas fa-check"></i> Campeonato Mundial de Balonismo Michigan 2012</span></li>
					</ul>
					<ul>
						<li><span><i class="fas fa-check"></i> Campeonato Mundial de Balonismo Rio Claro - Brasil 2014</span></li>
						<li><span><i class="fas fa-check"></i> Pré Campeonato Mundial de Balonismo Saga – Japão 2015</span></li>
						<li><span><i class="fas fa-check"></i> World Air Games – Dubai 2015</span></li>
					</ul>
				</div>
			</div>
		</div>


	</div>


	<?php include('partials/balonismo.php'); ?>
	<?php include('partials/newsletter.php'); ?>
	<?php include_once('footer.php') ?>
