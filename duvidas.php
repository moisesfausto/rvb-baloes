<?php require_once('header.php'); ?>
<?php include_once('partials/diferenciais.php'); ?>

<!-- breadcrumb-area start -->
<div class="breadcrumb-area main_ts_duvidas">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="breadcrumb_box text-center">
					<h2 class="breadcrumb-title">Dúvidas </h2>
					<!-- breadcrumb-list start -->
					<ul class="breadcrumb-list">
						<li class="breadcrumb-item"><a href="index.html">HOME</a></li>
						<li class="breadcrumb-item active">DÚVIDAS </li>
					</ul>
					<!-- breadcrumb-list end -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- breadcrumb-area end -->












<div id="main-wrapper">
	<div class="site-wrapper-reveal main_duvidas_frequentes">
		<!--====================  Accordion area ====================-->
		<div class="accordion-wrapper section-space--ptb_100">
			<div class="container">

				<div class="row">
					<div class="col-lg-12">
						<!-- section-title-wrap Start -->
						<div class="section-title-wrap text-center section-space--mb_20">
							<h3 class="heading">Dúvidas Frequentes</h3>
						</div>
						<!-- section-title-wrap Start -->
					</div>
				</div>

				<div class="row">
					<div class="col-lg-6">
						<div class="faq-wrapper section-space--mt_40">
							<div id="accordion">


								<div class="card">
									<div class="card-header">
										<h5 class="mb-0">
											<button class="btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false">
												As Tendas Infláveis tem motor (moto-ventilador)? <span><i class="far fa-chevron-circle-down"></i>
													<i class="far fa-chevron-circle-up"></i> </span>
											</button>
										</h5>
									</div>
									<div id="collapseTwo" class="collapse" data-parent="#accordion">
										<div class="card-body">
											<p>Mitech takes into consideration every little detail to make sure the system run smoothly and
												responsively. Mitech employs a new technique called Minified Technology for securing customers'
												database & building up highly confidential firewalls. </p>
										</div>
									</div>
								</div>


								<div class="card">
									<div class="card-header">
										<h5 class="mb-0">
											<button class="btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false">
												Como funciona a garantia do Tenda Inflável? <span> <i class="far fa-chevron-circle-down"></i>
													<i class="far fa-chevron-circle-up"></i> </span>
											</button>
										</h5>
									</div>
									<div id="collapseThree" class="collapse" data-parent="#accordion">
										<div class="card-body">
											<p>We reduce redundant complex calculations and lengthy erroneous code texts with simpler ones to
												ensure Mitech would run seamlessly and the design is reserved in its best form when viewed from
												a wide range of mobile devices & browsers. </p>
										</div>
									</div>
								</div>
								<div class="card">
									<div class="card-header">
										<h5 class="mb-0">
											<button class="collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false">
												Como posso limpar minha Tenda Inflável? <span> <i class="far fa-chevron-circle-down"></i>
													<i class="far fa-chevron-circle-up"></i> </span>
											</button>
										</h5>
									</div>
									<div id="collapseFour" class="collapse" data-parent="#accordion">
										<div class="card-body">
											<p>Our service offerings to enhance customer experience throughout the product lifecycle includes
												– test and repair, service management, and end-to-end warranty management. </p>
										</div>
									</div>
								</div>
								<div class="card">
									<div class="card-header">
										<h5 class="mb-0">
											<button class="collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false">
												Quais os cuidados se deve ter para instalar a Tenda Inflável ? <span> <i class="far fa-chevron-circle-down"></i>
													<i class="far fa-chevron-circle-up"></i> </span>
											</button>
										</h5>
									</div>
									<div id="collapseFive" class="collapse" data-parent="#accordion">
										<div class="card-body">
											<p>Our service offerings to enhance customer experience throughout the product lifecycle includes
												– test and repair, service management, and end-to-end warranty management. </p>
										</div>
									</div>
								</div>
								<div class="card">
									<div class="card-header">
										<h5 class="mb-0">
											<button class="collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false">
												Onde posso guardar o meu inflável? <span> <i class="far fa-chevron-circle-down"></i>
													<i class="far fa-chevron-circle-up"></i> </span>
											</button>
										</h5>
									</div>
									<div id="collapseSix" class="collapse" data-parent="#accordion">
										<div class="card-body">
											<p>Our service offerings to enhance customer experience throughout the product lifecycle includes
												– test and repair, service management, and end-to-end warranty management. </p>
										</div>
									</div>
								</div>
								<div class="card">
									<div class="card-header">
										<h5 class="mb-0">
											<button class="collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false">
												Qual o nível de barulho (ruído) gerado pelo motor do inflável promocional? <span> <i class="far fa-chevron-circle-down"></i>
													<i class="far fa-chevron-circle-up"></i> </span>
											</button>
										</h5>
									</div>
									<div id="collapseSeven" class="collapse" data-parent="#accordion">
										<div class="card-body">
											<p>Our service offerings to enhance customer experience throughout the product lifecycle includes
												– test and repair, service management, and end-to-end warranty management. </p>
										</div>
									</div>
								</div>
								<div class="card">
									<div class="card-header">
										<h5 class="mb-0">
											<button class="collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false">
												As Tendas Infláveis tem motor (moto-ventilador)? <span> <i class="far fa-chevron-circle-down"></i>
													<i class="far fa-chevron-circle-up"></i> </span>
											</button>
										</h5>
									</div>
									<div id="collapseEight" class="collapse" data-parent="#accordion">
										<div class="card-body">
											<p>Our service offerings to enhance customer experience throughout the product lifecycle includes
												– test and repair, service management, and end-to-end warranty management. </p>
										</div>
									</div>
								</div>
								<div class="card">
									<div class="card-header">
										<h5 class="mb-0">
											<button class="collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false">
												Como funciona a garantia do Tenda Inflável? <span> <i class="far fa-chevron-circle-down"></i>
													<i class="far fa-chevron-circle-up"></i> </span>
											</button>
										</h5>
									</div>
									<div id="collapseNine" class="collapse" data-parent="#accordion">
										<div class="card-body">
											<p>Our service offerings to enhance customer experience throughout the product lifecycle includes
												– test and repair, service management, and end-to-end warranty management. </p>
										</div>
									</div>
								</div>
								<div class="card">
									<div class="card-header">
										<h5 class="mb-0">
											<button class="collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false">
												Como posso limpar minha Tenda Inflável? <span> <i class="far fa-chevron-circle-down"></i>
													<i class="far fa-chevron-circle-up"></i> </span>
											</button>
										</h5>
									</div>
									<div id="collapseTen" class="collapse" data-parent="#accordion">
										<div class="card-body">
											<p>Our service offerings to enhance customer experience throughout the product lifecycle includes
												– test and repair, service management, and end-to-end warranty management. </p>
										</div>
									</div>
								</div>
								<div class="card">
									<div class="card-header">
										<h5 class="mb-0">
											<button class="collapsed" data-toggle="collapse" data-target="#collapseEleven" aria-expanded="false">
												Quais os cuidados se deve ter para instalar a Tenda Inflável ? <span> <i class="far fa-chevron-circle-down"></i>
													<i class="far fa-chevron-circle-up"></i> </span>
											</button>
										</h5>
									</div>
									<div id="collapseEleven" class="collapse" data-parent="#accordion">
										<div class="card-body">
											<p>Our service offerings to enhance customer experience throughout the product lifecycle includes
												– test and repair, service management, and end-to-end warranty management. </p>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>

					<div class="col-lg-6">
						<!-- Start toggles -->
						<div class="toggles-wrapper section-space--mt_40">
							<div class="faq-wrapper">
								<div id="faq-toggles">
									<div class="card">
										<div class="card-header">
											<h5 class="mb-0">
												<button class="btn-link collapsed" data-toggle="collapse" data-target="#faq_two" aria-expanded="false">
													Quando tempo demora para a Tenda Inflável encher?<span> <i class="far fa-chevron-circle-down"></i>
														<i class="far fa-chevron-circle-up"></i> </span>
												</button>
											</h5>
										</div>
										<div id="faq_two" class="collapse">
											<div class="card-body">
												<p>Mitech takes into consideration every little detail to make sure the system run smoothly and
													responsively. Mitech employs a new technique called Minified Technology for securing
													customers' database & building up highly confidential firewalls. </p>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header">
											<h5 class="mb-0">
												<button class="btn-link collapsed" data-toggle="collapse" data-target="#faq_three" aria-expanded="false">
													Como funciona a garantia do motor? <span> <i class="far fa-chevron-circle-down"></i>
														<i class="far fa-chevron-circle-up"></i> </span>
												</button>
											</h5>
										</div>
										<div id="faq_three" class="collapse">
											<div class="card-body">
												<p>We reduce redundant complex calculations and lengthy erroneous code texts with simpler ones
													to ensure Mitech would run seamlessly and the design is reserved in its best form when viewed
													from a wide range of mobile devices & browsers. </p>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header">
											<h5 class="mb-0">
												<button class="collapsed" data-toggle="collapse" data-target="#faq_five" aria-expanded="false">
													Quanto tempo dura a Tenda Inflável ? <span> <i class="far fa-chevron-circle-down"></i>
														<i class="far fa-chevron-circle-up"></i> </span>
												</button>
											</h5>
										</div>
										<div id="faq_five" class="collapse">
											<div class="card-body">
												<p>We are available to freelance hiring with on-demand extra services, including WordPress site
													design/ redesign, WordPress installation, all-in-one customization, video production, video
													editing and stop motion video producing.</p>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header">
											<h5 class="mb-0">
												<button class="collapsed" data-toggle="collapse" data-target="#faq_six" aria-expanded="false">
													Como armazenar o minha Tenda Inflável? <span> <i class="far fa-chevron-circle-down"></i>
														<i class="far fa-chevron-circle-up"></i> </span>
												</button>
											</h5>
										</div>
										<div id="faq_six" class="collapse">
											<div class="card-body">
												<p>We are available to freelance hiring with on-demand extra services, including WordPress site
													design/ redesign, WordPress installation, all-in-one customization, video production, video
													editing and stop motion video producing.</p>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header">
											<h5 class="mb-0">
												<button class="collapsed" data-toggle="collapse" data-target="#faq_seven" aria-expanded="false">
													Qual a voltagem utilizada no motor do inflável promocional? <span> <i class="far fa-chevron-circle-down"></i>
														<i class="far fa-chevron-circle-up"></i> </span>
												</button>
											</h5>
										</div>
										<div id="faq_seven" class="collapse">
											<div class="card-body">
												<p>We are available to freelance hiring with on-demand extra services, including WordPress site
													design/ redesign, WordPress installation, all-in-one customization, video production, video
													editing and stop motion video producing.</p>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header">
											<h5 class="mb-0">
												<button class="collapsed" data-toggle="collapse" data-target="#faq_eight" aria-expanded="false">
													É possível encher o meu inflável com um gerador de energia? <span> <i class="far fa-chevron-circle-down"></i>
														<i class="far fa-chevron-circle-up"></i> </span>
												</button>
											</h5>
										</div>
										<div id="faq_eight" class="collapse">
											<div class="card-body">
												<p>We are available to freelance hiring with on-demand extra services, including WordPress site
													design/ redesign, WordPress installation, all-in-one customization, video production, video
													editing and stop motion video producing.</p>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header">
											<h5 class="mb-0">
												<button class="collapsed" data-toggle="collapse" data-target="#faq_nine" aria-expanded="false">
													Quando tempo demora para a Tenda Inflável encher? <span> <i class="far fa-chevron-circle-down"></i>
														<i class="far fa-chevron-circle-up"></i> </span>
												</button>
											</h5>
										</div>
										<div id="faq_nine" class="collapse">
											<div class="card-body">
												<p>We are available to freelance hiring with on-demand extra services, including WordPress site
													design/ redesign, WordPress installation, all-in-one customization, video production, video
													editing and stop motion video producing.</p>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header">
											<h5 class="mb-0">
												<button class="collapsed" data-toggle="collapse" data-target="#faq_ten" aria-expanded="false">
													Como funciona a garantia do motor? <span> <i class="far fa-chevron-circle-down"></i>
														<i class="far fa-chevron-circle-up"></i> </span>
												</button>
											</h5>
										</div>
										<div id="faq_ten" class="collapse">
											<div class="card-body">
												<p>We are available to freelance hiring with on-demand extra services, including WordPress site
													design/ redesign, WordPress installation, all-in-one customization, video production, video
													editing and stop motion video producing.</p>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header">
											<h5 class="mb-0">
												<button class="collapsed" data-toggle="collapse" data-target="#faq_eleven" aria-expanded="false">
													Quanto tempo dura a Tenda Inflável ? <span> <i class="far fa-chevron-circle-down"></i>
														<i class="far fa-chevron-circle-up"></i> </span>
												</button>
											</h5>
										</div>
										<div id="faq_eleven" class="collapse">
											<div class="card-body">
												<p>We are available to freelance hiring with on-demand extra services, including WordPress site
													design/ redesign, WordPress installation, all-in-one customization, video production, video
													editing and stop motion video producing.</p>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header">
											<h5 class="mb-0">
												<button class="collapsed" data-toggle="collapse" data-target="#faq_twelve" aria-expanded="false">
													Como armazenar o minha Tenda Inflável? <span> <i class="far fa-chevron-circle-down"></i>
														<i class="far fa-chevron-circle-up"></i> </span>
												</button>
											</h5>
										</div>
										<div id="faq_twelve" class="collapse">
											<div class="card-body">
												<p>We are available to freelance hiring with on-demand extra services, including WordPress site
													design/ redesign, WordPress installation, all-in-one customization, video production, video
													editing and stop motion video producing.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- End toggles -->
					</div>
				</div>
			</div>
		</div>
		<!--====================  Accordion area  ====================-->




	</div>


	<?php include('partials/newsletter.php'); ?>
	<?php require_once('footer.php'); ?>
