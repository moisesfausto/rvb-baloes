<?php require_once('header.php'); ?>
<?php include_once('partials/diferenciais.php'); ?>

<!-- breadcrumb-area start -->
<div class="breadcrumb-area main_ts_inflaveis_promocionais">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="breadcrumb_box text-center">
					<h2 class="breadcrumb-title">Infláveis Promocionais</h2>
					<!-- breadcrumb-list start -->
					<ul class="breadcrumb-list">
						<li class="breadcrumb-item"><a href="index.html">Home</a></li>
						<li class="breadcrumb-item active">INFLÁVEIS PROMOCIONAIS</li>
					</ul>
					<!-- breadcrumb-list end -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- breadcrumb-area end -->

<div id="main-wrapper">
	<div class="site-wrapper-reveal">

		<!-- ============ Tenda inflaveis Start =============== -->
		<div class="machine-learning-service-area machine-learning-service-bg section-space--ptb_100">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<!-- section-title-wrap Start -->
						<div class="section-title-wrap text-center section-space--mb_30">
							<h3 class="heading"><span class="text-color-spotlight">Tendas Infláveis RVB</span>,
								um diferencial <br> para suas Ações Promocionais</h3>
						</div>
						<!-- section-title-wrap Start -->
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="feature-images__five">
							<div class="row">

								<div class="col-lg-4 col-md-6 wow move-up animated" style="visibility: visible;">
									<!-- ht-box-icon Start -->
									<div class="ht-box-images style-05">
										<div class="image-box-wrap">
											<div class="box-image">
												<img class="img-fulid"
													src="assets/images/inflaveis-promocionais/tenda-inflavel-promocional-3x3-metros-personalizada-vonder-ferramentas.jpg"
													alt="Barraca ou Stand Inflável">
											</div>
											<div class="content">
												<h5 class="heading">Barraca ou Stand Inflável</h5>
												<div class="text">
													Tenda Inflável também conhecida como Barraca ou Stand Inflável é a solução perfeita para suas
													Ações Promocionais. No seu negócio ela é capaz de aumentar suas vendas quando usada de forma
													estratégica atraindo a atenção dos seus clientes para sua produto ou marca. Perfeita em
													eventos em geral devido seu alto impacto visual. Muito usada como ponto de venda itinerante
													por ser fácil de montar e transportar.
												</div>
											</div>
										</div>
									</div>
									<!-- ht-box-icon End -->
								</div>

								<div class="col-lg-4 col-md-6 wow move-up animated" style="visibility: visible;">
									<!-- ht-box-icon Start -->
									<div class="ht-box-images style-05">
										<div class="image-box-wrap">
											<div class="box-image">
												<img class="img-fulid"
													src="assets/images/inflaveis-promocionais/Farmacia-Baloes-Promocionais-Inflaveis-Droga-Raia.jpg"
													alt="As tendas infláveis são versáteis">
											</div>
											<div class="content">
												<h5 class="heading">As tendas infláveis são versáteis</h5>
												<div class="text">
													As tendas infláveis são versáteis podem ser utilizadas em varias ações promocionais
													incrementando as vendas imediatamente e, por serem personalizadas, divulgam sua marca ou
													produto para seus clientes. Em eventos itinerantes proporciona um espaço diferenciado para
													atender seu público! Fabricamos nas medidas de 3×3, 4×4, 5×5, 6×6 ou fazemos sob medida.
												</div>
											</div>
										</div>
									</div>
									<!-- ht-box-icon End -->
								</div>

								<div class="col-lg-4 col-md-6 wow move-up animated" style="visibility: visible;">
									<!-- ht-box-icon Start -->
									<div class="ht-box-images style-05">
										<div class="image-box-wrap">
											<div class="box-image">
												<img class="img-fulid"
													src="assets/images/inflaveis-promocionais/Shows-Baloes-Promocionais-Inflaveis-Dilsinho.jpg"
													alt="Tem várias configurações">
											</div>
											<div class="content">
												<h5 class="heading">Tem várias configurações</h5>
												<div class="text">
													A Tenda Inflável é uma propaganda que leva as cores da sua empresa juntamente com sua
													logomarca. Tem várias configurações como fechamento das laterais (cortinas), balcões,
													medalhão, banners entre outros. Já vem com motor embutido e compacto com baixo consumo de
													energia.
												</div>
											</div>
										</div>
									</div>
									<!-- ht-box-icon End -->
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!-- ============ Tenda inflaveis end =============== -->


		<div class="main_galeria_de_imagens main_inflaveis_promocionais_imagens">
			<div class="feature-large-images-wrapper section-space--ptb_100">
				<div class="container">

					<div class="row">
						<div class="col-lg-12">
							<!-- section-title-wrap Start -->
							<div class="section-title-wrap text-center section-space--mb_30">
								<h6 class="section-sub-title mb-20">GALERIA DE IMAGENS</h6>
								<h3 class="heading">Nossos resultados são a demonstração <br> de toda atenção dedicação.</h3>
							</div>
							<!-- section-title-wrap Start -->
						</div>
					</div>

					<div class="row">
						<div class="col-12">
							<div class="row small-mt__30">

								<div class="col-lg-3 col-md-6 mt-30">
									<!-- Box large image warap Start -->
									<a href="#" class="box-large-image__two">
										<div class="box-large-image__two__box">
											<div class="box-large-image__midea">
												<div class="single-gallery__thum bg-item-images bg-img"
													data-bg="assets/images/box-image/droga-raia.jpg"
													style="background-image: url(&quot;assets/images/box-image/droga-raia.jpg&quot;);"></div>
											</div>

											<div class="box-info">
												<h5 class="heading">DrogaRaia</h5>
												<!-- <div class="box-more-info">
														<div class="text">We provide the most responsive and functional IT design for companies and
															businesses worldwide.</div>
														<div class="btn">
															<i class="button-icon far fa-long-arrow-right"></i>
														</div>
													</div> -->
											</div>
										</div>
									</a>
									<!-- Box large image warap End -->
								</div>

								<div class="col-lg-6 col-md-6  mt-30">
									<!-- Box large image warap Start -->
									<a href="#" class="box-large-image__two">
										<div class="box-large-image__two__box">
											<div class="box-large-image__midea">
												<div class="single-gallery__thum bg-item-images bg-img"
													data-bg="assets/images/box-image/wizard.jpg"
													style="background-image: url(&quot;assets/images/box-image/wizard.jpg&quot;);"></div>
											</div>

											<div class="box-info">
												<h5 class="heading">Herbalife</h5>
											</div>
										</div>
									</a>
									<!-- Box large image warap End -->
								</div>

								<div class="col-lg-3 col-md-6 mt-30">
									<!-- Box large image warap Start -->
									<a href="#" class="box-large-image__two">
										<div class="box-large-image__two__box">
											<div class="box-large-image__midea">
												<div class="single-gallery__thum bg-item-images bg-img"
													data-bg="assets/images/box-image/herbalife.jpg"
													style="background-image: url(&quot;assets/images/box-image/herbalife.jpg&quot;);"></div>
											</div>

											<div class="box-info">
												<h5 class="heading">Herbalife</h5>
											</div>
										</div>
									</a>
									<!-- Box large image warap End -->
								</div>

								<div class="col-lg-4 col-md-6 mt-30">
									<!-- Box large image warap Start -->
									<a href="#" class="box-large-image__two">
										<div class="box-large-image__two__box">
											<div class="box-large-image__midea">
												<div class="single-gallery__thum bg-item-images bg-img"
													data-bg="assets/images/box-image/now.jpg"
													style="background-image: url(&quot;assets/images/box-image/now.jpg&quot;);"></div>
											</div>

											<div class="box-info">
												<h5 class="heading">Net Now</h5>
											</div>
										</div>
									</a>
									<!-- Box large image warap End -->
								</div>

								<div class="col-lg-4 col-md-6 mt-30">
									<!-- Box large image warap Start -->
									<a href="#" class="box-large-image__two">
										<div class="box-large-image__two__box">
											<div class="box-large-image__midea">
												<div class="single-gallery__thum bg-item-images bg-img"
													data-bg="assets/images/box-image/ciesp.jpg"
													style="background-image: url(&quot;assets/images/box-image/ciesp.jpg&quot;);"></div>
											</div>

											<div class="box-info">
												<h5 class="heading">Ciesp</h5>
											</div>
										</div>
									</a>
									<!-- Box large image warap End -->
								</div>

								<div class="col-lg-4 col-md-6 mt-30">
									<!-- Box large image warap Start -->
									<a href="#" class="box-large-image__two">
										<div class="box-large-image__two__box">
											<div class="box-large-image__midea">
												<div class="single-gallery__thum bg-item-images bg-img"
													data-bg="assets/images/box-image/fiat.jpg"
													style="background-image: url(&quot;assets/images/box-image/fiat.jpg&quot;);"></div>
											</div>

											<div class="box-info">
												<h5 class="heading">Fiat</h5>
											</div>
										</div>
									</a>
									<!-- Box large image warap End -->
								</div>

							</div>
							<div class="col-lg-12">
								<div class="feature-list-button-box mt-40 text-center">
									<a href="#" class="ht-btn ht-btn-md">Veja Mais</a>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>


		<div class="main_vantangens">
			<div class="container">

				<h2>Vantagens das nossas Tendas Infláveis</h2>

				<div class="row mt-40 mb-40">
					<div class="col-md-12 col-lg-6">
						<ul>
							<li><i class="far fa-check-circle"></i> Alto impacto visual;</li>
							<li><i class="far fa-check-circle"></i> Excelente custo-benefício;</li>
							<li><i class="far fa-check-circle"></i> Impressão digital em alta resolução com proteção UV;</li>
							<li><i class="far fa-check-circle"></i> Quando vazio, se torna um pacote compacto, que representa
								aproximadamente 10% do seu tamanho inflado, excelente para transportar e armazenar;</li>
							<li><i class="far fa-check-circle"></i> Flexibilidade para usar em vários e diferentes tipos de eventos;
							</li>
							<li><i class="far fa-check-circle"></i> Baixo custo de operação;</li>
						</ul>
					</div>
					<div class="col-md-12 col-lg-6">
						<ul>
							<li><i class="far fa-check-circle"></i> Destacar-se frente à concorrência;</li>
							<li><i class="far fa-check-circle"></i> Totalmente personalizado, em formato, tamanho, cor e impressão
								digital;</li>
							<li><i class="far fa-check-circle"></i> Portátil e simples de montar e desmontar, economizando tempo e
								trabalho;</li>
							<li><i class="far fa-check-circle"></i> Podem ser utilizados em ambientes abertos ou fechados, dia ou
								noite;</li>
							<li><i class="far fa-check-circle"></i> Baixo consumo de energia;</li>
							<li><i class="far fa-check-circle"></i> Aumento nas vendas no seu PDV;</li>
						</ul>
					</div>
				</div>

			</div>
		</div>


		<div id="main-wrapper ">
			<div class="site-wrapper-reveal main_diferenciais_promocionais">
				<!--====== Carousel Sliders Wrapper Start =======-->
				<div class="carousel-sliders-wrapper bg-gray section-space--ptb_100">
					<div class="container">
						<h2>Diferenciais das nossas tendas</h2>
						<div class="col-12 col-md-10" style="margin: 0 auto;">
							<h4>Nossas Tendas Infláveis são produzidas com materiais de qualidade por profissionais com 20 anos de
								experiencia,
								que garantem o menor prazo de entrega do mercado. As tendas da RVB tem motor embutido,
								iluminação interna com grade de proteção, acompanham acessórios como: sacola com identificação,
								extensão elétrica e kit para pequenos reparos e como opcional estacas de aço.</h4>
						</div>

						<div class="row">
							<div class="col-lg-12">
								<div class="swiper-container carousel-slider__container">
									<div class="swiper-wrapper">
										<div class="swiper-slide">
											<!-- Carousel Slider Start -->
											<div class="carousel-slider wow move-up">
												<div class="carousel-slider__image">
													<img class="img-fluid"
														src="assets/images/inflaveis-promocionais/extensao-com-5-metros-e-kit-para-reparos-dos-inflaveis.jpg"
														alt="Extensão com 5 metros e kit para pequenos reparos">
												</div>
												<div class="carousel-slider__content text-center">
													<div class="text">
														Extensão com 5 metros e kit para pequenos reparos
													</div>
												</div>
											</div>
											<!-- Carousel Slider End -->
										</div>
										<div class="swiper-slide">
											<!-- Carousel Slider Start -->
											<div class="carousel-slider wow move-up">
												<div class="carousel-slider__image">
													<img class="img-fluid"
														src="assets/images/inflaveis-promocionais/tenda-inflavel-rvb-com-iluminacao-interna-com-grade-de-protecao-farmais.jpg"
														alt="Ponto de iluminação interna com grade de proteção">
												</div>
												<div class="carousel-slider__content text-center">
													<div class="text">
														Ponto de iluminação interna com grade de proteção
													</div>
												</div>
											</div>
											<!-- Carousel Slider End -->
										</div>
										<div class="swiper-slide">
											<!-- Carousel Slider Start -->
											<div class="carousel-slider wow move-up">
												<div class="carousel-slider__image">
													<img class="img-fluid"
														src="assets/images/inflaveis-promocionais/motor-do-inflavel-embutido.jpg"
														alt="Moto ventilador embutido">
												</div>
												<div class="carousel-slider__content text-center">
													<div class="text">
														Moto ventilador embutido
													</div>
												</div>
											</div>
											<!-- Carousel Slider End -->
										</div>
									</div>
									<div class="swiper-pagination swiper-pagination-9 section-space--mt_50"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--====== Carousel Sliders Wrapper End ======-->

				<div class="main_como_usar">
					<div class="container">
						<h2>Como usar as Tendas Infláveis</h2>
						<div class="row align-items-center">
							<div class="col-md-12 col-lg-6">
								<img src="assets/images/inflaveis-promocionais/Baloes-Inflaveis-Personalizados-Tenda-Strong.jpg"
									alt="Como usar as Tendas Infláveis" class="img-fluid">
							</div>
							<div class="col-md-12 col-lg-6">
								<div class="list_como_usar">
									<ul>
										<li><i class="fas fa-check"></i> Como Pontos de Vendas (PDV) itinerantes;</li>
										<li><i class="fas fa-check"></i> Em frente de Lojas e Comércios;</li>
										<li><i class="fas fa-check"></i> Muita usada como Mini ShowRoom em grandes redes de varejo.;</li>
										<li><i class="fas fa-check"></i> Inaugurações de Farmácias, Perfumarias, Pet Shoppings e Óticas;
										</li>
										<li><i class="fas fa-check"></i> Em eventos em Lojas de Suplementos e Academias;</li>
										<li><i class="fas fa-check"></i> Ações Promocionais em Feirões, Concessionárias e Revendas de
											Automóveis;</li>
										<li><i class="fas fa-check"></i> Ações Promocionais em Escolas, Colégios e Faculdades;</li>
										<li><i class="fas fa-check"></i> Apoio para ações de Trade Marketing;</li>
										<li><i class="fas fa-check"></i> Eventos com Food Trucks.</li>
										<li><i class="fas fa-check"></i> Imobiliárias, Shoppings, Postos de combustíveis, Supermercados,
											Lojas, etc;</li>
										<li><i class="fas fa-check"></i> Ponto de apoio em Competições e Eventos Esportivos;</li>
										<li><i class="fas fa-check"></i> Divulgação em Campanhas Publicitárias, Marketing e Políticas;</li>
										<li><i class="fas fa-check"></i> Ações Promocionais em Feiras Agropecuárias;</li>
										<li><i class="fas fa-check"></i> Lançamentos de Empreendimentos Imobiliários.</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>


				<div class="main_satisfacao">
					<div class="testimonial-slider-area section-space--ptb_100">
						<div class="container">
							<div class="row">
								<div class="col-lg-12">
									<div class="section-title-wrap text-center section-space--mb_40">
										<h6 class="section-sub-title mb-20">SATISFAÇÃO</h6>
										<h3 class="heading">Depoimentos do clientes!</h3>
									</div>
									<div class="testimonial-slider">
										<div
											class="swiper-container testimonial-slider__container swiper-container-initialized swiper-container-horizontal">
											<div class="swiper-wrapper testimonial-slider__wrapper"
												style="transform: translate3d(-1200px, 0px, 0px); transition-duration: 0ms;">

												<div class="swiper-slide swiper-slide-active" data-swiper-slide-index="0"
													style="width: 570px; margin-right: 30px;">
													<div class="testimonial-slider__one wow move-up animated" style="visibility: visible;">

														<div class="testimonial-slider--info">
															<div class="testimonial-slider__media">
																<img src="https://via.placeholder.com/90x90"
																	class="img-fluid" alt="Larissa">
															</div>

															<div class="testimonial-slider__author">
																<div class="testimonial-rating">
																	<span class="fa fa-star"></span>
																	<span class="fa fa-star"></span>
																	<span class="fa fa-star"></span>
																	<span class="fa fa-star"></span>
																	<span class="fa fa-star"></span>
																</div>
																<div class="author-info">
																	<h6 class="name">Larissa Oliveira</h6>
																	<span class="designation">Marketing Grupo Bandeirantes</span>
																</div>
															</div>
														</div>

														<div class="testimonial-slider__text">“Gostaria de agradecer por toda a atenção no
															atendimento desde o orçamento até o trabalho de pós-venda. Nossas tendas ficaram ótimas,
															atenderam às nossas necessidades e expectativas.</div>

													</div>
												</div>


												<div class="swiper-slide swiper-slide-next swiper-slide-duplicate-prev"
													data-swiper-slide-index="1" style="width: 570px; margin-right: 30px;">
													<div class="testimonial-slider__one wow move-up animated" style="visibility: visible;">

														<div class="testimonial-slider--info">
															<div class="testimonial-slider__media">
																<img src="https://via.placeholder.com/90x90"
																	class="img-fluid" alt="Leandro">
															</div>

															<div class="testimonial-slider__author">
																<div class="testimonial-rating">
																	<span class="fa fa-star"></span>
																	<span class="fa fa-star"></span>
																	<span class="fa fa-star"></span>
																	<span class="fa fa-star"></span>
																	<span class="fa fa-star"></span>
																</div>
																<div class="author-info">
																	<h6 class="name">Leandro Lima</h6>
																	<span class="designation">Diretor Regional Farma Conde</span>
																</div>
															</div>
														</div>

														<div class="testimonial-slider__text">“Gostaria de agradecer por toda a atenção no
															atendimento desde o orçamento até o trabalho de pós-venda. Nossas tendas ficaram ótimas,
															atenderam às nossas necessidades e expectativas.</div>

													</div>
												</div>

											</div>
											<div
												class="swiper-pagination swiper-pagination-t01 section-space--mt_30 swiper-pagination-clickable swiper-pagination-bullets">
												<span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0"
													role="button" aria-label="Go to slide 1"></span><span class="swiper-pagination-bullet"
													tabindex="0" role="button" aria-label="Go to slide 2"></span></div>
											<span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="price_tenda_inflaveis">
					<div class="container">
						<div class="row">
							<div class="col-md-12 col-lg-8">
								<h2>Preço das Tendas Infláveis</h2>
								<p>Por ser Personalizada, o preço da tenda inflável depende principalmente do seu tamanho,
									modelo, impressão digital e configuração de fechamento das laterais (cortinas), balcões, medalhão,
									banners entre outros. Solicite um orçamento para nossos consultores, informando onde e como será
									utilizado,
									para oferecermos a tenda inflável ideal a sua necessidade com o Melhor Preço.</p>
								<h3>Como montar e desmontar sua <br> Tenda Inflável</h3>
								<img
									src="assets/images/inflaveis-promocionais/passo-a-passo-de-como-montar-a-tenda-inflavel-da-rvb-baloes.png"
									alt="Como montar e desmontar sua Tenda Inflável" class="img-fluid">
							</div>
							<div class="col-md-12 col-lg-4">
								<form action="">

								</form>
							</div>
						</div>
					</div>
				</div>


				<!-- Flexible image slider wrapper Start -->
				<div class="main_video_montagem">
					<div class="testimonial-slider-area section-space--ptb_100 bg-gray">
						<div class="container">
							<div class="row">
								<div class="col-lg-12">
									<div class="section-title-wrap text-center section-space--mb_40">
										<h3 class="heading">Veja como é simples a montagem e o alto impacto visual</h3>
									</div>
									<div class="testimonial-slider">
										<div
											class="swiper-container testimonial-slider__container swiper-container-initialized swiper-container-horizontal">
											<div class="swiper-wrapper testimonial-slider__wrapper"
												style="transform: translate3d(-1200px, 0px, 0px); transition-duration: 0ms;">

												<div class="swiper-slide swiper-slide-active" data-swiper-slide-index="0"
													style="width: 570px; margin-right: 30px;">
													<div class="testimonial-slider__one wow move-up animated" style="visibility: visible;">
														<img src="assets/images/inflaveis-promocionais/play.png" alt="" class="img-fluid">
													</div>
													<h6>Tenda Inflável Promocional com 3×3 metros, personalizada para escola de idiomas WIZARD
													</h6>
												</div>


												<div class="swiper-slide swiper-slide-active" data-swiper-slide-index="1"
													style="width: 570px; margin-right: 30px;">
													<div class="testimonial-slider__one wow move-up animated" style="visibility: visible;">
														<img src="assets/images/inflaveis-promocionais/play.png" alt="" class="img-fluid">
													</div>
													<h6>Tenda Inflável Promocional com 4×4 metros com medalhão, personalizada para BAND.</h6>
												</div>

											</div>
										</div>

										<div
											class="swiper-pagination swiper-pagination-t01 section-space--mt_30 swiper-pagination-clickable swiper-pagination-bullets">
											<span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button"
												aria-label="Go to slide 1"></span><span class="swiper-pagination-bullet" tabindex="0"
												role="button" aria-label="Go to slide 2"></span></div>

										<span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Flexible image slider wrapper End -->
			<div class="main_modelos_tenda">
				<div class="container">
					<h3>Modelos de Tendas Infláveis</h3>
					<h6>(clique nas imagens para abrir a galeria e ver todos os modelos)</h6>
					<div class="row">

						<div class="col-sm-12 col-md-6 col-lg-4">
							<img src="assets/images/inflaveis-promocionais/modelos/Tenda-Inflavel-Personalizada-3x3-STARRETT.jpg"
								alt="Tendas Inflável Personagens 3X3 STARRETT" class="img-fluid">
							<h4>Tendas Inflável Personagens 3X3 STARRETT</h4>
						</div>
						<div class="col-sm-12 col-md-6 col-lg-4">
							<img src="assets/images/inflaveis-promocionais/modelos/Tenda-Inflavel-Promocional-3x3-Pitagoras.jpg"
								alt="Tenda Inflável Personalizado 3X3 PITÁGORAS" class="img-fluid">
							<h4>Tenda Inflável Personalizado 3X3 PITÁGORAS</h4>
						</div>
						<div class="col-sm-12 col-md-6 col-lg-4">
							<img src="assets/images/inflaveis-promocionais/modelos/Tenda-Inflavel-Personalizada-3x3-MICRO-CHIP.jpg"
								alt="Tenda Inflável Personalizado 3X3 MICRO CHIP" class="img-fluid">
							<h4>Tenda Inflável Personalizado 3X3 MICRO CHIP</h4>
						</div>

					</div>
					<div class="row">

						<div class="col-sm-12 col-md-6 col-lg-4">
							<img src="assets/images/inflaveis-promocionais/modelos/Tenda-Inflavel-Personalizada-3x3-WHISKAS.jpg"
								alt="Tenda Inflável Personalizada 3X3 WHISKAS" class="img-fluid">
							<h4>Tenda Inflável Personalizada 3X3 WHISKAS</h4>
						</div>
						<div class="col-sm-12 col-md-6 col-lg-4">
							<img src="assets/images/inflaveis-promocionais/modelos/Tenda-Inflavel-Personalizada-3x3-NATURALIS.jpg"
								alt="Tenda Inflável Personalizadas 3X3 NATURALIS" class="img-fluid">
							<h4>Tenda Inflável Personalizadas 3X3 NATURALIS</h4>
						</div>
						<div class="col-sm-12 col-md-6 col-lg-4">
							<img
								src="assets/images/inflaveis-promocionais/modelos/Tenda-Inflavel-Personalizada-3x3-FARMACIAS-REDE-USIFARMA.jpg"
								alt="Tenda Inflável Personalizadas 3X3 FARMÁCIAS REDE USIFARMA" class="img-fluid">
							<h4>Tenda Inflável Personalizadas 3X3 FARMÁCIAS REDE USIFARMA</h4>
						</div>

					</div>
					<div class="row">

						<div class="col-sm-12 col-md-6 col-lg-4">
							<img src="assets/images/inflaveis-promocionais/modelos/Tenda-Inflavel-Personalizada-3x3-DROGALAGOS.jpg"
								alt="Tenda Inflável Personalizada 3x3 DROGALAGOS" class="img-fluid">
							<h4>Tenda Inflável Personalizada 3x3 DROGALAGOS</h4>
						</div>
						<div class="col-sm-12 col-md-6 col-lg-4">
							<img
								src="assets/images/inflaveis-promocionais/modelos/Tenda-Inflavel-Personalizada-3x3-RADIO-TROPICAL.jpg"
								alt="Tenda Inflável Personalizada 3X3 RADIO TROPICAL" class="img-fluid">
							<h4>Tenda Inflável Personalizada 3X3 RADIO TROPICAL</h4>
						</div>
						<div class="col-sm-12 col-md-6 col-lg-4">
							<img
								src="assets/images/inflaveis-promocionais/modelos/Tenda-Inflavel-Personalizada-3x3-COMERCIAL-GERDAU.jpg"
								alt="Tenda Inflável Personalizada 3X3 COMERCIAL GERDAU" class="img-fluid">
							<h4>Tenda Inflável Personalizada 3X3 COMERCIAL GERDAU</h4>
						</div>

					</div>
				</div>
			</div>

			<div class="main_tabela_cores">
				<div class="container">
					<h2>Tabela de cores dos tecidos dos infláveis promocionais desenvolvidos pela RVB Balões.</h2>
					<div class="row">
						<div class="col-12">
							<img src="assets/images/inflaveis-promocionais/tabela-de-cores-tecido-inflaveis-promocionais-rvb.jpg"
								alt="Tabela de cores dos tecidos dos infláveis promocionais desenvolvidos pela RVB Balões"
								class="img-fluid mt-40">
						</div>
					</div>
				</div>
			</div>

			<div class="accordion-wrapper main_question_promocionais section-space--ptb_100">
				<div class="container">

					<div class="row">
						<div class="col-lg-12">
							<!-- section-title-wrap Start -->
							<div class="section-title-wrap text-center section-space--mb_20">
								<h3 class="heading">Perguntas mais frequentes</h3>
							</div>
							<!-- section-title-wrap Start -->
						</div>
					</div>

					<div class="row">
						<div class="col-lg-6">
							<div class="faq-wrapper section-space--mt_40">
								<div id="accordion">


									<div class="card">
										<div class="card-header">
											<h5 class="mb-0">
												<button class="btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo"
													aria-expanded="false">
													As Tendas Infláveis tem motor (moto-ventilador)? <span><i
															class="far fa-chevron-circle-down"></i>
														<i class="far fa-chevron-circle-up"></i> </span>
												</button>
											</h5>
										</div>
										<div id="collapseTwo" class="collapse" data-parent="#accordion" style="">
											<div class="card-body">
												<p>Sim. O Inflável é fabricado em tecido e costurado, logo o motor precisa ficar ligado o tempo
													todo.</p>
											</div>
										</div>
									</div>


									<div class="card">
										<div class="card-header">
											<h5 class="mb-0">
												<button class="btn-link collapsed" data-toggle="collapse" data-target="#collapseThree"
													aria-expanded="false">
													Como funciona a garantia do Tenda Inflável? <span> <i class="far fa-chevron-circle-down"></i>
														<i class="far fa-chevron-circle-up"></i> </span>
												</button>
											</h5>
										</div>
										<div id="collapseThree" class="collapse" data-parent="#accordion">
											<div class="card-body">
												<p>We reduce redundant complex calculations and lengthy erroneous code texts with simpler ones
													to
													ensure Mitech would run seamlessly and the design is reserved in its best form when viewed
													from
													a wide range of mobile devices &amp; browsers. </p>
											</div>
										</div>
									</div>

									<div class="card">
										<div class="card-header">
											<h5 class="mb-0">
												<button class="collapsed" data-toggle="collapse" data-target="#collapseFour"
													aria-expanded="false">
													Como posso limpar minha Tenda Inflável? <span> <i class="far fa-chevron-circle-down"></i>
														<i class="far fa-chevron-circle-up"></i> </span>
												</button>
											</h5>
										</div>
										<div id="collapseFour" class="collapse" data-parent="#accordion">
											<div class="card-body">
												<p>Our service offerings to enhance customer experience throughout the product lifecycle
													includes
													– test and repair, service management, and end-to-end warranty management. </p>
											</div>
										</div>
									</div>

									<div class="card">
										<div class="card-header">
											<h5 class="mb-0">
												<button class="collapsed" data-toggle="collapse" data-target="#collapseFive"
													aria-expanded="false">
													Quais os cuidados se deve ter para instalar a Tenda Inflável ? <span> <i
															class="far fa-chevron-circle-down"></i>
														<i class="far fa-chevron-circle-up"></i> </span>
												</button>
											</h5>
										</div>
										<div id="collapseFive" class="collapse" data-parent="#accordion">
											<div class="card-body">
												<p>Our service offerings to enhance customer experience throughout the product lifecycle
													includes
													– test and repair, service management, and end-to-end warranty management. </p>
											</div>
										</div>
									</div>

									<div class="card">
										<div class="card-header">
											<h5 class="mb-0">
												<button class="collapsed" data-toggle="collapse" data-target="#collapseSix"
													aria-expanded="false">
													Onde posso guardar o meu inflável? <span> <i class="far fa-chevron-circle-down"></i>
														<i class="far fa-chevron-circle-up"></i> </span>
												</button>
											</h5>
										</div>
										<div id="collapseSix" class="collapse" data-parent="#accordion">
											<div class="card-body">
												<p>Our service offerings to enhance customer experience throughout the product lifecycle
													includes
													– test and repair, service management, and end-to-end warranty management. </p>
											</div>
										</div>
									</div>

									<div class="card">
										<div class="card-header">
											<h5 class="mb-0">
												<button class="collapsed" data-toggle="collapse" data-target="#collapseSeven"
													aria-expanded="false">
													Qual o nível de barulho (ruído) gerado pelo motor do inflável promocional? <span> <i
															class="far fa-chevron-circle-down"></i>
														<i class="far fa-chevron-circle-up"></i> </span>
												</button>
											</h5>
										</div>
										<div id="collapseSeven" class="collapse" data-parent="#accordion">
											<div class="card-body">
												<p>Our service offerings to enhance customer experience throughout the product lifecycle
													includes
													– test and repair, service management, and end-to-end warranty management. </p>
											</div>
										</div>
									</div>

								</div>

							</div>
						</div>

						<div class="col-lg-6">
							<!-- Start toggles -->
							<div class="toggles-wrapper section-space--mt_40">
								<div class="faq-wrapper">
									<div id="faq-toggles">
										<div class="card">
											<div class="card-header">
												<h5 class="mb-0">
													<button class="btn-link collapsed" data-toggle="collapse" data-target="#faq_two"
														aria-expanded="false">
														Quando tempo demora para a Tenda Inflável encher?<span> <i
																class="far fa-chevron-circle-down"></i>
															<i class="far fa-chevron-circle-up"></i> </span>
													</button>
												</h5>
											</div>
											<div id="faq_two" class="collapse">
												<div class="card-body">
													<p>Mitech takes into consideration every little detail to make sure the system run smoothly
														and
														responsively. Mitech employs a new technique called Minified Technology for securing
														customers' database &amp; building up highly confidential firewalls. </p>
												</div>
											</div>
										</div>
										<div class="card">
											<div class="card-header">
												<h5 class="mb-0">
													<button class="btn-link collapsed" data-toggle="collapse" data-target="#faq_three"
														aria-expanded="false">
														Como funciona a garantia do motor? <span> <i class="far fa-chevron-circle-down"></i>
															<i class="far fa-chevron-circle-up"></i> </span>
													</button>
												</h5>
											</div>
											<div id="faq_three" class="collapse">
												<div class="card-body">
													<p>We reduce redundant complex calculations and lengthy erroneous code texts with simpler ones
														to ensure Mitech would run seamlessly and the design is reserved in its best form when
														viewed
														from a wide range of mobile devices &amp; browsers. </p>
												</div>
											</div>
										</div>

										<div class="card">
											<div class="card-header">
												<h5 class="mb-0">
													<button class="btn-link collapsed" data-toggle="collapse" data-target="#faq_for"
														aria-expanded="false">
														Quanto tempo dura a Tenda Inflável ? <span> <i class="far fa-chevron-circle-down"></i>
															<i class="far fa-chevron-circle-up"></i> </span>
													</button>
												</h5>
											</div>
											<div id="faq_for" class="collapse">
												<div class="card-body">
													<p>We reduce redundant complex calculations and lengthy erroneous code texts with simpler ones
														to ensure Mitech would run seamlessly and the design is reserved in its best form when
														viewed
														from a wide range of mobile devices &amp; browsers. </p>
												</div>
											</div>
										</div>

										<div class="card">
											<div class="card-header">
												<h5 class="mb-0">
													<button class="btn-link collapsed" data-toggle="collapse" data-target="#faq_five"
														aria-expanded="false">
														Como armazenar o minha Tenda Inflável? <span> <i class="far fa-chevron-circle-down"></i>
															<i class="far fa-chevron-circle-up"></i> </span>
													</button>
												</h5>
											</div>
											<div id="faq_five" class="collapse">
												<div class="card-body">
													<p>We reduce redundant complex calculations and lengthy erroneous code texts with simpler ones
														to ensure Mitech would run seamlessly and the design is reserved in its best form when
														viewed
														from a wide range of mobile devices &amp; browsers. </p>
												</div>
											</div>
										</div>

										<div class="card">
											<div class="card-header">
												<h5 class="mb-0">
													<button class="btn-link collapsed" data-toggle="collapse" data-target="#faq_six"
														aria-expanded="false">
														Qual a voltagem utilizada no motor do inflável promocional? <span> <i
																class="far fa-chevron-circle-down"></i>
															<i class="far fa-chevron-circle-up"></i> </span>
													</button>
												</h5>
											</div>
											<div id="faq_six" class="collapse">
												<div class="card-body">
													<p>We reduce redundant complex calculations and lengthy erroneous code texts with simpler ones
														to ensure Mitech would run seamlessly and the design is reserved in its best form when
														viewed
														from a wide range of mobile devices &amp; browsers. </p>
												</div>
											</div>
										</div>

										<div class="card">
											<div class="card-header">
												<h5 class="mb-0">
													<button class="btn-link collapsed" data-toggle="collapse" data-target="#faq_seven"
														aria-expanded="false">
														É possível encher o meu inflável com um gerador de energia? <span> <i
																class="far fa-chevron-circle-down"></i>
															<i class="far fa-chevron-circle-up"></i> </span>
													</button>
												</h5>
											</div>
											<div id="faq_seven" class="collapse">
												<div class="card-body">
													<p>We reduce redundant complex calculations and lengthy erroneous code texts with simpler ones
														to ensure Mitech would run seamlessly and the design is reserved in its best form when
														viewed
														from a wide range of mobile devices &amp; browsers. </p>
												</div>
											</div>
										</div>

									</div>
								</div>
							</div>
							<!-- End toggles -->
						</div>
					</div>
				</div>
			</div>

		</div>


		<?php include_once('partials/balonismo.php'); ?>
		<?php include('partials/newsletter.php'); ?>
		<?php include_once('footer.php'); ?>
