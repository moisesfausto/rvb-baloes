		<!--====================  footer area ====================-->
		<div class="footer-area-wrapper">
			<div class="footer-area section-space--ptb_80">
				<div class="container">
					<div class="row footer-widget-wrapper">
						<div class="col-lg-4 col-md-6 col-sm-6 footer-widget">
							<div class="footer-widget__logo mb-30">
								<img src="assets/images/logo/rvb-baloes.png" class="img-fluid" alt="RVB Balões">
							</div>
							<ul class="footer-widget__list">
								<li>Rua Moises Marx, 1093 - Vila Matilde</li>
								<li>São Paulo/SP - Brasil</li>
								<li class="mb-30">CEP. 03507-000 +55</li>
								<li class="mb-30"><a href="tel:+551120928787" class="hover-style-link">(11) 2092-8787</a></li>
								<li class="mb-30">Segunda à sexta: 08h as 17h</li>
								<li>Horário de almoço: 12h as 13h</li>
							</ul>
						</div>
						<div class="col-lg-2 col-md-4 col-sm-6 footer-widget mt-30">
							<h6 class="footer-widget__title mb-20">Navegação</h6>
							<ul class="footer-widget__list">
								<li><a href="#" class="hover-style-link">Home</a></li>
								<li><a href="#" class="hover-style-link">Sobre</a></li>
								<li><a href="#" class="hover-style-link">Infláveis</a></li>
								<li><a href="#" class="hover-style-link">Promocionais</a></li>
								<li><a href="#" class="hover-style-link">Balonismo</a></li>
								<li><a href="#" class="hover-style-link">Galeria</a></li>
								<li><a href="#" class="hover-style-link">Blog da RVB</a></li>
								<li><a href="#" class="hover-style-link">Contato</a></li>
								<li><a href="#" class="hover-style-link">Dúvidas</a></li>
							</ul>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-6 footer-widget mt-30 main_ts_about">
							<h6 class="footer-widget__title mb-20">Sobre</h6>
							<p class="mb-30">
								A RVB Balões está no mercado há 20 anos
								fabricando infláveis e balões promocionais
								para ações de Marketing e Vendas.
							</p>
							<p>
								Os Infláveis Promocionais são uma ferramenta
								de marketing e vendas altamente efetiva,
								com um alto impacto visual e uma ótima facilidade
								de uso. São totalmente customizáveis em formato,
								tamanho, cores e impressão digital.
							</p>
						</div>
						<div class="col-lg-2 col-md-4 col-sm-6 footer-widget main_ts_social_media mt-30">
							<h6 class="footer-widget__title mb-20">Redes Sociais</h6>
							<ul class="footer-widget__list">
								<li><a href="https://www.facebook.com/rvbbaloes/" target="_blank" class="text-decoration-none"><i class="fab fa-facebook-square"></i></a></li>
								<li><a href="https://twitter.com/rvbbaloes" target="_blank" class="text-decoration-none"><i class="fab fa-twitter-square"></i></a></li>
								<li><a href="https://www.instagram.com/rvbbaloes/" target="_blank" class="text-decoration-none"><i class="fab fa-instagram"></i></a></li>
								<li><a href="https://www.linkedin.com/company/rvbbaloes" target="_blank" class="text-decoration-none"><i class="fab fa-linkedin"></i></a></li>
								<li><a href="https://www.youtube.com/channel/UCg9lylFPGSpT2X8pHLMUPYg" target="_blank" class="text-decoration-none"><img src="assets/images/icons/youtube-icon.svg" alt="Youtube"></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-copyright-area section-space--pb_30">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-md-6 text-center text-md-left">
							<span class="copyright-text">&copy; 2020 - Direitos Reservados</a></span>
						</div>
						<div class="col-md-6 text-center text-md-right">
							<a href="http://www.agenciatsuru.com.br/?utm_source=RVB&utm_medium=Site&utm_campaign=desenvolvido_por" target="_blank" rel="noopener noreferrer">
								<span class="mr-4" style="font-size: 15px; color: #333333;">Desenvolvido pela</span>
								<img src="assets/images/logo/tsuru-logo.png" alt="Agencia Tsuru">
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--====================  End of footer area  ====================-->









	</div>
	<!--====================  scroll top ====================-->
	<a href="#" class="scroll-top" id="scroll-top">
		<i class="arrow-top fal fa-long-arrow-up"></i>
		<i class="arrow-bottom fal fa-long-arrow-up"></i>
	</a>
	<!--====================  End of scroll top  ====================-->


	<!--====================  mobile menu overlay ====================-->
	<div class="mobile-menu-overlay" id="mobile-menu-overlay">
		<div class="mobile-menu-overlay__inner">
			<div class="mobile-menu-overlay__header">
				<div class="container-fluid">
					<div class="row align-items-center">
						<div class="col-md-6 col-8">
							<!-- logo -->
							<div class="logo">
								<a href="index.html">
									<img src="assets/images/logo/rvb-baloes.png" class="img-fluid" alt="RVB Balões">
								</a>
							</div>
						</div>
						<div class="col-md-6 col-4">
							<!-- mobile menu content -->
							<div class="mobile-menu-content text-right">
								<span class="mobile-navigation-close-icon" id="mobile-menu-close-trigger"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="mobile-menu-overlay__body">
				<nav class="offcanvas-navigation">
					<ul>
						<li class=".has-children .has-children--multilevel-submenu">
							<a href="<?= $pathURL; ?>/index.php"><span>Home</span></a>
							<!-- <ul class="submenu">
								<li><a href="index-infotechno.html"><span>Infotechno</span></a></li>
							</ul> -->
						</li>

						<li>
							<a href="<?= $pathURL; ?>/nossa-historia.php"><span>RVB Balões</span></a>
						</li>
						<li class="has-children">
							<a href="<?= $pathURL; ?>/inflaveis-promocionais.php"><span>Infláveis Promocionais</span></a>
							<ul class="sub-menu">
								<li><a href="<?= $pathURL; ?>/inflaveis-promocionais-2.php"><span>Inflaveis Promocionais</span></a></li>
							</ul>
						</li>
						<li>
							<a href="<?= $pathURL; ?>/galeria-de-imagens.php"><span>Galeria</span></a>
						</li>
						<li>
							<a href="<?= $pathURL; ?>/balonismo.php"><span>Balonismo</span></a>
						</li>
						<li class="has-children">
							<a href="<?= $pathURL; ?>/blog.php"><span>Blog da RVB</span></a>
							<ul class="sub-menu">
								<li><a href="<?= $pathURL; ?>/single.php"><span>Post</span></a></li>
							</ul>
						</li>
						<li>
							<a href="<?= $pathURL; ?>/contato.php"><span>Contato</span></a>
						</li>
						<li>
							<a href="<?= $pathURL; ?>/duvidas.php"><span>Dúvidas</span></a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
	<!--====================  End of mobile menu overlay  ====================-->







	<!--====================  search overlay ====================-->
	<div class="search-overlay" id="search-overlay">

		<div class="search-overlay__header">
			<div class="container-fluid">
				<div class="row align-items-center">
					<div class="col-md-6 ml-auto col-4">
						<!-- search content -->
						<div class="search-content text-right">
							<span class="mobile-navigation-close-icon" id="search-close-trigger"></span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="search-overlay__inner">
			<div class="search-overlay__body">
				<div class="search-overlay__form">
					<form action="#">
						<input type="text" placeholder="Search">
					</form>
				</div>
			</div>
		</div>
	</div>
	<!--====================  End of search overlay  ====================-->

	<!-- JS
    ============================================ -->
	<!-- Modernizer JS -->
	<script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>

	<!-- jQuery JS -->
	<script src="assets/js/vendor/jquery-3.5.1.min.js"></script>
	<script src="assets/js/vendor/jquery-migrate-3.3.0.min.js"></script>

	<!-- Bootstrap JS -->
	<script src="assets/js/vendor/bootstrap.min.js"></script>

	<!-- wow JS -->
	<script src="assets/js/plugins/wow.min.js"></script>

	<!-- Swiper Slider JS -->
	<script src="assets/js/plugins/swiper.min.js"></script>

	<!-- Light gallery JS -->
	<script src="assets/js/plugins/lightgallery.min.js"></script>

	<!-- Waypoints JS -->
	<script src="assets/js/plugins/waypoints.min.js"></script>

	<!-- Counter down JS -->
	<!-- <script src="assets/js/plugins/countdown.min.js"></script> -->

	<!-- Isotope JS -->
	<script src="assets/js/plugins/isotope.min.js"></script>

	<!-- Masonry JS -->
	<!-- <script src="assets/js/plugins/masonry.min.js"></script> -->

	<!-- ImagesLoaded JS -->
	<script src="assets/js/plugins/images-loaded.min.js"></script>

	<!-- Wavify JS -->
	<!-- <script src="assets/js/plugins/wavify.js"></script> -->

	<!-- jQuery Wavify JS -->
	<!-- <script src="assets/js/plugins/jquery.wavify.js"></script> -->

	<!-- circle progress JS -->
	<!-- <script src="assets/js/plugins/circle-progress.min.js"></script> -->

	<!-- counterup JS -->
	<script src="assets/js/plugins/counterup.min.js"></script>

	<!-- animation text JS -->
	<!-- <script src="assets/js/plugins/animation-text.min.js"></script> -->

	<!-- Vivus JS -->
	<!-- <script src="assets/js/plugins/vivus.min.js"></script> -->

	<!-- Some plugins JS -->
	<script src="assets/js/plugins/some-plugins.js"></script>


	<!-- Plugins JS (Please remove the comment from below plugins.min.js for better website load performance and remove plugin js files from avobe) -->


	<!-- <script src="assets/js/plugins/plugins.min.js"></script>  -->


	<!-- Main JS -->
	<script src="assets/js/main.js"></script>


</body>

</html>
