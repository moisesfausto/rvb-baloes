<div class="main_balonismo bg-gray">
				<div class="accordion-wrapper section-space--ptb_100">
					<div class="container-fluid">
						<div class="row align-items-center">
							<div class="col-lg-6">
								<div class="faq-wrapper faq-custom-col">

									<div class="section-title section-space--mb_40">
										<h6 class="section-sub-title mb-20">Balonismo</h6>
										<h3 class="heading mb-30">A RVB Balões participa das principais competições <br> de balonismo a nível nacional e internacional.</h3>
										<p>A RVB Balões está no mercado há 20 anos fabricando infláveis e <br> balões promocionais para ações de Marketing e Vendas.</p>
										<div class="feature-list-button-box mt-30">
											<a href="#" class="ht-btn ht-btn-md">Veja Mais</a>
										</div>
									</div>

								</div>
							</div>

							<div class="col-lg-6">
								<div class="rv-video-section">

									<div class="ht-banner-01 ">
										<img class="img-fluid border-radus-5 animation_images one wow fadeInDown animated"
											src="assets/images/bg/home/ballon-1.jpg" alt=""
											style="visibility: visible; animation-name: animateUpDown;">
									</div>

									<div class="ht-banner-02">
										<img class="img-fluid border-radus-5 animation_images two wow fadeInDown animated"
											src="assets/images/bg/home/ballon-3.jpg" alt=""
											style="visibility: visible; animation-name: animateUpDown;">
									</div>

									<div class="ht-banner-03">
										<img class="img-fluid border-radus-5 animation_images three wow fadeInDown animated"
											src="assets/images/bg/home/ballon-2.jpg" alt=""
											style="visibility: visible; animation-name: animateUpDown;">
									</div>

								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
