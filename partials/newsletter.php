<div class="main_newsletter bg-theme-four">
    <div class="container">
			<div class="row align-items-end">
        <div class="col-md-5">
            <h6>Assine nossa newsletter<br><strong>Receba nossos conteúdos com prioridade direto em seu e-mail</strong></h6>
        </div>
        <div class="col-md-7 retorno">
            <form name="newsletter-topo-site" id="form_newsletter_topo_site" class="tsuru_form" method="" action="">
							<div class="form-row align-items-end">
								<div class="col">
									<label for="nome">Nome</label>
									<input type="text" class="form-control">
								</div>
								<div class="col">
									<label for="email">E-mail</label>
									<input type="email" class="form-control">
								</div>
								<div class="col">
									<button type="submit">Assinar agora</button>
								</div>
							</div>

            </form>
        </div>
			</div>
    </div>
	</div>
