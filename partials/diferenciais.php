<!--===========  diferenciais  Start =============-->
<div class="main_ts_diferencias">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul>
								<li><img src="assets/images/icons/diferenciais/envio.png" alt="Enviamos para todo o Brasil"></li>
								<div class="divider d-none d-md-block"></div>
								<li><img src="assets/images/icons/diferenciais/21-anos.png" alt="21 Anos"></li>
								<div class="divider d-none d-md-block"></div>
								<li><img src="assets/images/icons/diferenciais/desconto.png"
										alt="Até 10% de desconto para pagamento a vista"></li>
								<div class="divider d-none d-md-block"></div>
								<li><img src="assets/images/icons/diferenciais/sem-juros.png" alt="em até 10x sem juros "></li>
								<div class="divider d-none d-md-block"></div>
								<li><img src="assets/images/icons/diferenciais/producao.png" alt="produção em tempo recorde"></li>
								<div class="divider d-none d-md-block"></div>

								<li><img src="assets/images/icons/diferenciais/layout.png" alt="Layout 3D incluido"></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!--===========  diferenciais  end =============-->
