<?php require_once('header.php'); ?>
<?php include_once('partials/diferenciais.php'); ?>

<!-- breadcrumb-area start -->
<div class="breadcrumb-area main_ts_bg_blog">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="breadcrumb_box text-center">
					<h2 class="breadcrumb-title">Blog da RVB</h2>
					<!-- breadcrumb-list start -->
					<ul class="breadcrumb-list">
						<li class="breadcrumb-item"><a href="index.html">HHOME</a></li>
						<li class="breadcrumb-item active">BLOG DA RVB</li>
					</ul>
					<!-- breadcrumb-list end -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- breadcrumb-area end -->


<div id="main-wrapper">
	<div class="site-wrapper-reveal">

		<!--====================   Blog Area Start ====================-->
		<div class="blog-pages-wrapper section-space--ptb_100">
			<div class="container">
				<div class="row">

					<div class="col-lg-12 order-lg-2 order-1">
						<div class="section-title-wrap text-center section-space--mb_60">
							<h3 class="heading"><span class="text-color-spotlight">Fique</span> por dentro</h3>
						</div>
						<div class="main-blog-wrap">


							<?php for ($post=0; $post <= 6; $post++): ?>
							<!--======= Single Blog Item Start ========-->
							<div class="single-blog-item post-list-wrapper xs-list-blog-item wow move-up">
								<div class="row ">
									<div class="col-lg-6">
										<!-- Post Feature Start -->
										<div class="post-feature blog-thumbnail">
											<a href="#">
												<img class="img-fluid" src="https://via.placeholder.com/540x332" alt="Blog Images">
											</a>
										</div>
										<!-- Post Feature End -->
									</div>

									<div class="col-lg-6">
										<!-- Post info Start -->
										<div class="post-info lg-blog-post-info">
											<h4 class="post-title">
												<a href="#">Festival de Balonismo coloriu o céu de Resende</a>
											</h4>

											<div class="post-meta mt-20">
												<div class="post-author">
													<a href="#">
														<img class="img-fluid avatar-96" src="https://via.placeholder.com/30x30" alt="">Pedro Henrique</a>
												</div>
												<div class="post-date">
													<span class="far fa-calendar meta-icon"></span> 20 de Janeiro de 2021
												</div>
												<div class="post-view">
													<span class="meta-icon far fa-eye"></span>320 visualizações
												</div>
											</div>

											<div class="post-excerpt mt-15">
												<p>No último fim de semana foi impossível ignorar que havia algo diferente na paisagem de Resende.</p>
											</div>


											<div class="read-post-share-wrap mt-30">
												<div class="post-read-more">
													<a href="#" class="ht-btn ht-btn-md bg-spotlight">Veja Mais</a>
												</div>
											</div>
										</div>
										<!-- Post info End -->
									</div>
								</div>
							</div>
							<!--===== Single Blog Item End =========-->
							<?php endfor; ?>


							<div class="ht-pagination mt-30 pagination justify-content-center">
								<div class="pagination-wrapper">
									<ul class="page-pagination">
										<li><a class="prev page-numbers" href="#">ANTERIOR</a></li>
										<li><a class="page-numbers current" href="#">1</a></li>
										<li><a class="page-numbers" href="#">2</a></li>
										<li><a class="next page-numbers" href="#">PRÓXIMO</a></li>
									</ul>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
		<!--====================  Blog Area End  ====================-->



	</div>

	<?php include('partials/newsletter.php'); ?>
	<?php include_once('footer.php'); ?>
