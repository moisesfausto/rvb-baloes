<?php $pathURL = 'http://rvb.web284.uni5.net'; ?>

<!DOCTYPE html>
<html class="no-js" lang="zxx">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>RVB Balões</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Favicon -->
	<link rel="icon" href="https://www.rvb.com.br/wp-content/uploads/2016/05/favicon.jpg" type="image/jpeg">
	<link rel="shortcut icon" href="https://www.rvb.com.br/wp-content/uploads/2016/05/favicon.jpg" type="image/jpeg">

	<!-- CSS
        ============================================ -->

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="assets/css/vendor/bootstrap.min.css">

	<!-- Font Family CSS -->
	<link rel="stylesheet" href="assets/css/vendor/overpass.css">
	<link rel="stylesheet" href="assets/css/vendor/cerebrisans.css">

	<!-- FontAwesome CSS -->
	<link rel="stylesheet" href="assets/css/vendor/fontawesome-all.min.css">
	<link rel="stylesheet" href="assets/css/vendor/linea-icons.css">

	<!-- Swiper slider CSS -->
	<link rel="stylesheet" href="assets/css/plugins/swiper.min.css">

	<!-- animate-text CSS -->
	<link rel="stylesheet" href="assets/css/plugins/animate-text.css">

	<!-- Animate CSS -->
	<link rel="stylesheet" href="assets/css/plugins/animate.min.css">

	<!-- Light gallery CSS -->
	<link rel="stylesheet" href="assets/css/plugins/lightgallery.min.css">

	<!-- Vendor & Plugins CSS (Please remove the comment from below vendor.min.css & plugins.min.css for better website load performance and remove css files from avobe) -->

	<!-- <link rel="stylesheet" href="assets/css/vendor/vendor.min.css">
        <link rel="stylesheet" href="assets/css/plugins/plugins.min.css">
         -->
	<!-- Main Style CSS -->
	<link rel="stylesheet" href="assets/css/style.css">

</head>

<body>


	<div class="preloader-activate preloader-active open_tm_preloader">
		<div class="preloader-area-wrap">
			<div class="spinner d-flex justify-content-center align-items-center h-100">
				<div class="bounce1"></div>
				<div class="bounce2"></div>
				<div class="bounce3"></div>
			</div>
		</div>
	</div>


	<div class="header-area header-sticky d-md-none only-mobile-sticky">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="header position-relative">
						<!-- brand logo -->
						<div class="header__logo top-logo">
							<a href="index.html">
								<img src="assets/images/logo/rvb-baloes.png" width="85" height="85" alt="RVB Balões">
							</a>
						</div>

						<div class="header-right flexible-image-slider-wrap">

							<div class="header-right-inner" id="hidden-icon-wrapper">

								<!-- Header Top Info -->
								<span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
							</div>
						</div>
						<!-- mobile menu -->
						<div class="mobile-navigation-icon d-block d-xl-none" id="mobile-menu-trigger">
							<i></i>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--====================  header area ====================-->
	<div class="header-area header-sticky only-mobile-sticky d-none d-md-block">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="header position-relative">
						<!-- brand logo -->
						<div class="header__logo top-logo">
							<a href="index.html">
								<img src="assets/images/logo/rvb-baloes.png" width="85" height="85" class="img-fluid" alt="">
							</a>
						</div>

						<div class="mobile-navigation-icon d-block d-xl-none" id="mobile-menu-trigger">
							<i></i>
						</div>

						<div class="header-bottom-wrap bg-white d-md-block d-none header-sticky">
							<div class="container">
								<div class="row">
									<div class="col-lg-12">

										<div class="main_ts_search">
											<form action="" method="post">
												<input name="s" type="text" placeholder="O que você procura?">
												<i class="fas fa-search"></i>
											</form>
										</div>

										<div class="header-bottom-inner">
											<div class="header-bottom-left-wrap">
												<!-- navigation menu -->
												<div class="header__navigation d-none d-xl-block">
													<nav class="navigation-menu navigation-menu--text_white">

														<ul>
															<li class=".has-children .has-children--multilevel-submenu">
																<a href="<?= $pathURL; ?>/index.php"><span>Home</span></a>
																<!-- <ul class="submenu">
																	<li><a href="index-infotechno.html"><span>Infotechno</span></a></li>
																</ul> -->
															</li>

															<li>
																<a href="<?= $pathURL; ?>/nossa-historia.php"><span>RVB Balões</span></a>
															</li>
															<li class="has-children has-children--multilevel-submenu">
																<a href="<?= $pathURL; ?>/inflaveis-promocionais.php"><span>Infláveis Promocionais</span></a>
																<ul class="submenu">
																	<li><a href="<?= $pathURL; ?>/inflaveis-promocionais-2.php"><span>Infláveis Promocionais</span></a></li>
																</ul>
															</li>
															<li>
																<a href="<?= $pathURL; ?>/galeria-de-imagens.php"><span>Galeria</span></a>
															</li>
															<li>
																<a href="<?= $pathURL; ?>/balonismo.php"><span>Balonismo</span></a>
															</li>
															<li class="has-children has-children--multilevel-submenu">
																<a href="<?= $pathURL; ?>/blog.php"><span>Blog da RVB</span></a>
																<ul class="submenu">
																	<li><a href="<?= $pathURL; ?>/single.php"><span>Post</span></a></li>
																</ul>
															</li>
															<li>
																<a href="<?= $pathURL; ?>/contato.php"><span>Contato</span></a>
															</li>
															<li>
																<a href="<?= $pathURL; ?>/duvidas.php"><span>Dúvidas</span></a>
															</li>
														</ul>
													</nav>
												</div>
											</div>


										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<!--====================  End of header area  ====================-->

	<!--====================  header baloes ====================-->

	<div class="main_ts_nav_baloes">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul>
						<a href="">
							<li>
								<img src="assets\images\icons\navbar-baloes\tendas.svg" alt="tendas">
								<span>TENDAS</span>
							</li>
						</a>
						<a href="">
							<li>
								<img src="assets\images\icons\navbar-baloes\baloes.svg" alt="baloes">
								<span>BALÃO</span>
							</li>
						</a>
						<a href="">
							<li>
								<img src="assets\images\icons\navbar-baloes\portal.svg" alt="portaç">
								<span>PORTAL</span>
							</li>
						</a>
						<a href="">
							<li>
								<img src="assets\images\icons\navbar-baloes\painel.svg" alt="painel">
								<span>PAINEL</span>
							</li>
						</a>
						<a href="">
							<li>
								<img src="assets\images\icons\navbar-baloes\totem.svg" alt="totem">
								<span>TOTEM</span>
							</li>
						</a>
						<a href="">
							<li>
								<img src="assets\images\icons\navbar-baloes\lata.svg" alt="lata">
								<span>LATA</span>
							</li>
						</a>
						<a href="">
							<li>
								<img src="assets\images\icons\navbar-baloes\garrafa.svg" alt="garrafa">
								<span>GARRAFA</span>
							</li>
						</a>
						<a href="">
							<li>
								<img src="assets\images\icons\navbar-baloes\replica.svg" alt="replica">
								<span>RÉPLICA</span>
							</li>
						</a>
						<a href="">
							<li>
								<img src="assets\images\icons\navbar-baloes\pote.svg" alt="pote">
								<span>POTE</span>
							</li>
						</a>
						<a href="">
							<li>
								<img src="assets\images\icons\navbar-baloes\tunel.svg" alt="tunel">
								<span>TÚNEL</span>
							</li>
						</a>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<!--====================  end header baloes ====================-->
