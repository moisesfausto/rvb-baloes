<?php require_once('header.php'); ?>
<?php include_once('partials/diferenciais.php'); ?>

<!-- breadcrumb-area start -->
<div class="breadcrumb-area main_ts_bg_galeria_imagens">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="breadcrumb_box text-center">
					<h1 class="breadcrumb-title">Galeria de Imagens</h1>
					<!-- breadcrumb-list start -->
					<ul class="breadcrumb-list">
						<li class="breadcrumb-item"><a href="index.html">HOME</a></li>
						<li class="breadcrumb-item active">GALERIA DE IMAGENS</li>
					</ul>
					<!-- breadcrumb-list end -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- breadcrumb-area end -->





<div id="main-wrapper">
	<div class="site-wrapper-reveal main_galeria_imagens">
		<!--======== Tabs Wrapper Start ======== -->
		<div class="tabs-wrapper bg-gray section-space--ptb_100">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="section-title-wrapper text-center section-space--mb_60 wow move-up">
							<h3 class="section-title"><span class="text-color-spotlight">Resultados</span> que deram <span
									class="text-color-spotlight">retorno</span></h3>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-12 ht-tab-wrap">
						<div class="text-center  ">
							<ul class="nav justify-content-center ht-tab-menu ht-tab-menu_two" role="tablist">
								<li class="tab__item nav-item active">
									<a class="nav-link active" id="nav-tab1" data-toggle="tab" href="#tab_list_01" role="tab"
										aria-selected="true">TENDAS</a>
								</li>
								<li class="tab__item nav-item">
									<a class="nav-link" id="nav-tab2" data-toggle="tab" href="#tab_list_02">BALÃO</a>
								</li>
								<li class="tab__item nav-item">
									<a class="nav-link" id="nav-tab3" data-toggle="tab" href="#tab_list_03">PORTAL</a>
								</li>
								<li class="tab__item nav-item">
									<a class="nav-link" id="nav-tab4" data-toggle="tab" href="#tab_list_04">PAINEL</a>
								</li>
								<li class="tab__item nav-item">
									<a class="nav-link" id="nav-tab5" data-toggle="tab" href="#tab_list_05">TOTEM</a>
								</li>
								<li class="tab__item nav-item">
									<a class="nav-link" id="nav-tab6" data-toggle="tab" href="#tab_list_06">LATA</a>
								</li>
								<li class="tab__item nav-item">
									<a class="nav-link" id="nav-tab7" data-toggle="tab" href="#tab_list_07">GARRAFA</a>
								</li>
								<li class="tab__item nav-item ">
									<a class="nav-link" id="nav-tab8" data-toggle="tab" href="#tab_list_08">RÉPLICA</a>
								</li>
								<li class="tab__item nav-item">
									<a class="nav-link" id="nav-tab9" data-toggle="tab" href="#tab_list_09">POTE</a>
								</li>
								<li class="tab__item nav-item">
									<a class="nav-link" id="nav-tab10" data-toggle="tab" href="#tab_list_10">TÚNEL</a>
								</li>
							</ul>
						</div>

						<div class="tab-content ht-tab__content">

							<div class="tab-pane fade show active" id="tab_list_01" role="tabpanel">
								<div class="tab-history-wrap section-space--mt_40">
									<div class="row">
										<div class="col-12 col-md-6 col-lg-4">
											<div class="wrap_resultados">
												<img
													src="assets/images/galeria-de-imagens/tenda-inflavel-promocional-3x3-metros-personalizada-vonder-ferramentas.jpg"
													alt="Barraca ou Stand Inflável" class="img-fluid">
												<span>TENDAS</span>
												<h6>Barraca ou Stand Inflável</h6>
											</div>
										</div>
										<div class="col-12 col-md-6 col-lg-4">
											<div class="wrap_resultados">
												<img
													src="assets/images/galeria-de-imagens/Farmacia-Baloes-Promocionais-Inflaveis-Droga-Raia.jpg"
													alt="As tendas infláveis são versáteis<" class="img-fluid">
												<span>TOTEM</span>
												<h6>As tendas infláveis são versáteis</h6>
											</div>
										</div>
										<div class="col-12 col-md-6 col-lg-4">
											<div class="wrap_resultados">
												<img src="assets/images/galeria-de-imagens/Shows-Baloes-Promocionais-Inflaveis-Dilsinho.jpg"
													alt="Tem várias configurações" class="img-fluid">
												<span>TOTEM</span>
												<h6>Tem várias configurações</h6>
											</div>
										</div>

										<div class="col-12 col-md-6 col-lg-4">
											<div class="wrap_resultados">
												<img
													src="assets/images/galeria-de-imagens/tenda-inflavel-promocional-3x3-metros-personalizada-vonder-ferramentas.jpg"
													alt="Barraca ou Stand Inflável" class="img-fluid">
												<span>TENDAS</span>
												<h6>Barraca ou Stand Inflável</h6>
											</div>
										</div>
										<div class="col-12 col-md-6 col-lg-4">
											<div class="wrap_resultados">
												<img
													src="assets/images/galeria-de-imagens/Farmacia-Baloes-Promocionais-Inflaveis-Droga-Raia.jpg"
													alt="As tendas infláveis são versáteis<" class="img-fluid">
												<span>TOTEM</span>
												<h6>As tendas infláveis são versáteis</h6>
											</div>
										</div>
										<div class="col-12 col-md-6 col-lg-4">
											<div class="wrap_resultados">
												<img src="assets/images/galeria-de-imagens/Shows-Baloes-Promocionais-Inflaveis-Dilsinho.jpg"
													alt="Tem várias configurações" class="img-fluid">
												<span>TOTEM</span>
												<h6>Tem várias configurações</h6>
											</div>
										</div>
									</div>


								</div>
							</div>

							<div class="tab-pane fade" id="tab_list_02" role="tabpanel">
								<div class="tab-history-wrap section-space--mt_60">
									<div class="row">

										<div class="col-12 col-md-6 col-lg-4">
											<div class="wrap_resultados">
												<img
													src="assets/images/galeria-de-imagens/tenda-inflavel-promocional-3x3-metros-personalizada-vonder-ferramentas.jpg"
													alt="Barraca ou Stand Inflável" class="img-fluid">
												<span>TENDAS</span>
												<h6>Barraca ou Stand Inflável</h6>
											</div>
										</div>
										<div class="col-12 col-md-6 col-lg-4">
											<div class="wrap_resultados">
												<img
													src="assets/images/galeria-de-imagens/Farmacia-Baloes-Promocionais-Inflaveis-Droga-Raia.jpg"
													alt="As tendas infláveis são versáteis<" class="img-fluid">
												<span>TOTEM</span>
												<h6>As tendas infláveis são versáteis</h6>
											</div>
										</div>
										<div class="col-12 col-md-6 col-lg-4">
											<div class="wrap_resultados">
												<img src="assets/images/galeria-de-imagens/Shows-Baloes-Promocionais-Inflaveis-Dilsinho.jpg"
													alt="Tem várias configurações" class="img-fluid">
												<span>TOTEM</span>
												<h6>Tem várias configurações</h6>
											</div>
										</div>

									</div>
								</div>
							</div>

							<div class="tab-pane fade" id="tab_list_03" role="tabpanel">
								<div class="tab-history-wrap section-space--mt_60 brand-logo-slider__one">
									<div class="row">

										<div class="col-12 col-md-6 col-lg-4">
											<div class="wrap_resultados">
												<img
													src="assets/images/galeria-de-imagens/tenda-inflavel-promocional-3x3-metros-personalizada-vonder-ferramentas.jpg"
													alt="Barraca ou Stand Inflável" class="img-fluid">
												<span>TENDAS</span>
												<h6>Barraca ou Stand Inflável</h6>
											</div>
										</div>
										<div class="col-12 col-md-6 col-lg-4">
											<div class="wrap_resultados">
												<img
													src="assets/images/galeria-de-imagens/Farmacia-Baloes-Promocionais-Inflaveis-Droga-Raia.jpg"
													alt="As tendas infláveis são versáteis<" class="img-fluid">
												<span>TOTEM</span>
												<h6>As tendas infláveis são versáteis</h6>
											</div>
										</div>
										<div class="col-12 col-md-6 col-lg-4">
											<div class="wrap_resultados">
												<img src="assets/images/galeria-de-imagens/Shows-Baloes-Promocionais-Inflaveis-Dilsinho.jpg"
													alt="Tem várias configurações" class="img-fluid">
												<span>TOTEM</span>
												<h6>Tem várias configurações</h6>
											</div>
										</div>

									</div>
								</div>
							</div>

							<div class="tab-pane fade" id="tab_list_03" role="tabpanel">
								<div class="tab-history-wrap section-space--mt_20">
									<div class="row">

										<div class="col-12 col-md-6 col-lg-4">
											<div class="wrap_resultados">
												<img
													src="assets/images/galeria-de-imagens/tenda-inflavel-promocional-3x3-metros-personalizada-vonder-ferramentas.jpg"
													alt="Barraca ou Stand Inflável" class="img-fluid">
												<span>TENDAS</span>
												<h6>Barraca ou Stand Inflável</h6>
											</div>
										</div>
										<div class="col-12 col-md-6 col-lg-4">
											<div class="wrap_resultados">
												<img
													src="assets/images/galeria-de-imagens/Farmacia-Baloes-Promocionais-Inflaveis-Droga-Raia.jpg"
													alt="As tendas infláveis são versáteis<" class="img-fluid">
												<span>TOTEM</span>
												<h6>As tendas infláveis são versáteis</h6>
											</div>
										</div>
										<div class="col-12 col-md-6 col-lg-4">
											<div class="wrap_resultados">
												<img src="assets/images/galeria-de-imagens/Shows-Baloes-Promocionais-Inflaveis-Dilsinho.jpg"
													alt="Tem várias configurações" class="img-fluid">
												<span>TOTEM</span>
												<h6>Tem várias configurações</h6>
											</div>
										</div>

									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>

		<!--======== Tabs Wrapper End ======== -->

	</div>

	<?php include('partials/newsletter.php'); ?>
	<?php include_once('footer.php'); ?>
