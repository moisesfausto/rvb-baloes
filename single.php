<?php require_once('header.php'); ?>
<?php include_once('partials/diferenciais.php'); ?>

<!-- breadcrumb-area start -->
<div class="breadcrumb-area main_ts_bg_blog">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="breadcrumb_box text-center">
					<h2 class="breadcrumb-title">Blog da RVB</h2>
					<!-- breadcrumb-list start -->
					<ul class="breadcrumb-list">
						<li class="breadcrumb-item"><a href="index.html">Home</a></li>
						<li class="breadcrumb-item active">Blog da RVB</li>
					</ul>
					<!-- breadcrumb-list end -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- breadcrumb-area end -->

<div id="main-wrapper">
	<div class="site-wrapper-reveal">


		<!--====================  Blog Area Start ====================-->
		<div class="blog-pages-wrapper main_ts_single section-space--ptb_100">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<!-- Post Feature Start -->
						<div class="post-feature blog-thumbnail  wow move-up">
							<img class="img-fluid" src="https://via.placeholder.com/1170x570" alt="Blog Images">
						</div>
						<!-- Post Feature End -->
					</div>
					<div class="col-lg-12 ml-auto mr-auto">
						<div class="main-blog-wrap">
							<!--======= Single Blog Item Start ========-->
							<div class="single-blog-item  wow move-up">

								<!-- Post info Start -->
								<div class="post-info lg-blog-post-info">

									<h3 class="post-title text-center">
										<a href="#">Festival de Balonismo coloriu o céu de Resende</a>
									</h3>

									<div class="post-meta mt-20 justify-content-center">
										<div class="post-author">
											<a href="#">
												<img class="img-fluid avatar-96" src="https://via.placeholder.com/30x30" alt=""> Pedro Henrique
											</a>
										</div>
										<div class="post-date">
											<span class="far fa-calendar meta-icon"></span> 20 de Janeiro de 2021
										</div>
										<div class="post-view">
											<span class="meta-icon far fa-eye"></span> 320 visualizações
										</div>
										<!-- <div class="post-comments">
											<span class="far fa-comment-alt meta-icon"></span>
											<a href="#" class="smooth-scroll-link">4 Comments</a>
										</div> -->
									</div>

									<div class="post-excerpt mt-15">
										<div class="post-content mx-3">
											<p>
											No último fim de semana foi impossível ignorar que havia algo diferente na paisagem de Resende.
											O município recebeu o Festival de Balonismo e enormes balões coloridos, reunindo balonistas e
											paraquedistas para um verdadeiro espetáculo. No último fim de semana foi impossívelignorar que havia
											algo diferente na paisagem de Resende. O município recebeu o Festival de Balonismo e enormes balões coloridos,
											reunindo balonistas e paraquedistas para um verdadeiro espetáculo.
											</p>
											<p>
											No último fim de semana foi impossível ignorar que havia algo diferente na paisagem de Resende.
											O município recebeu o Festival de Balonismo e enormes balões coloridos, reunindo balonistas e
											paraquedistas para um verdadeiro espetáculo. No último fim de semana foi impossível ignorar
											que havia algo diferente na paisagem de Resende. O município recebeu o Festival de Balonismo
											e enormes balões coloridos, reunindo balonistas e paraquedistas para um verdadeiro espetáculo.
											No último fim de semana foi impossívelignorar que havia algo diferente na paisagem de Resende.
											O município recebeu o Festival de Balonismo e enormes balões coloridos,
											reunindo balonistas e paraquedistas para um verdadeiro espetáculo. No último fim de semana
											foi impossível ignorar que havia algo diferente na paisagem de Resende. O município recebeu o
											Festival de Balonismo e enormes balões coloridos, reunindo balonistas e paraquedistas para um verdadeiro
											espetáculo.
											</p>
											<p>
											No último fim de semana foi impossível ignorar que havia algo diferente na paisagem de Resende.
											O município recebeu o Festival de Balonismo e enormes balões coloridos, reunindo balonistas e
											paraquedistas para um verdadeiro espetáculo. No último fim de semana foi impossívelignorar que
											havia algo diferente na paisagem de Resende. O município recebeu o Festival de Balonismo e
											enormes balões coloridos, reunindo balonistas e paraquedistas para um verdadeiro espetáculo.
											No último fim de semana foi impossível ignorar que havia algo diferente na paisagem de Resende.
											O município recebeu o Festival de Balonismo e enormes balões coloridos, reunindo balonistas
											e paraquedistas para um verdadeiro espetáculo.
											</p>
										</div>



										<div class="entry-post-share-wrap ">
											<div class="row align-items-center">
												<div class="col-6 col-md-6 col-lg-6">
													<div class="entry-post-tags">
														<div class="tagcloud-icon">
															<span class="far fa-comment-alt meta-icon"></span> 3 Comentários
														</div>
													</div>
												</div>

												<div class="col-6 col-md-6 col-lg-6">
													<div id="entry-post-share" class="entry-post-share">
														<div class="share-label">
															COMPARTILHAR
														</div>
														<div class="share-media">
															<span class="share-icon fas fa-share-alt"></span>

															<div class="share-list">
																<a class="hint--bounce hint--top hint--primary twitter" target="_blank"
																	aria-label="Twitter" href="#">
																	<i class="fab fa-twitter"></i>
																</a>
																<a class="hint--bounce hint--top hint--primary facebook" target="_blank"
																	aria-label="Facebook" href="#">
																	<i class="fab fa-facebook-f"></i>
																</a>
																<a class="hint--bounce hint--top hint--primary linkedin" target="_blank"
																	aria-label="Linkedin" href="#">
																	<i class="fab fa-linkedin"></i>
																</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>


										<div class="related-posts-wrapper">

											<div class="row">
												<div class="col-12">
													<h4 class="widget-title section-space--mb_50">Posts Relacionados </h4>
												</div>
												<div class="col-lg-6 mb-4 mb-sm-0">
													<!-- Single Valid Post Start -->
													<a class="single-valid-post-wrapper" href="#">
														<div class="single-blog__item">

															<div class="single-valid__thum bg-img" data-bg="https://via.placeholder.com/570x226"></div>

															<div class="post-content">
																<h6 class="post-title font-weight--bold">Festival de Balonismo coloriu o céu de Resende</h6>
															</div>

														</div>
													</a>
													<!-- Single Valid Post End -->
												</div>
												<div class="col-lg-6 mb-4 mb-sm-0">
													<!-- Single Valid Post Start -->
													<a class="single-valid-post-wrapper" href="#">
														<div class="single-blog__item">
															<div class="single-valid__thum bg-img" data-bg="https://via.placeholder.com/570x226">
															</div>

															<div class="post-content">
																<h6 class="post-title font-weight--bold">Festival de Balonismo coloriu o céu de Resende</h6>
															</div>

														</div>
													</a>
													<!-- Single Valid Post End -->
												</div>
											</div>
										</div>

										<div class="comment-list-wrapper">

											<div class="row">
												<div class="col-lg-12">
													<h4 class="widget-title section-space--mb_50">Comentários (3) </h4>
												</div>
												<div class="col-lg-12">

													<ol class="comment-list">
														<li class="comment">
															<div class="comment-2">
																<div class="comment-author vcard">
																	<img alt="" src="https://via.placeholder.com/88x88">
																</div>
																<div class="comment-content">
																	<div class="meta">
																		<h6 class="fn">Edna Watson</h6>
																	</div>
																	<div class="comment-text">
																		<p>Thanks for always keeping your HTML Template up to date. Your level of support
																			and dedication is second to none.</p>
																	</div>

																	<div class="comment-actions">
																		<div class="comment-datetime"> 20 de Janeiro de 2021 </div>
																	</div>
																</div>
															</div>
														</li><!-- comment End-->
														<li class="comment">
															<div class="comment-2">
																<div class="comment-author vcard">
																	<img alt="" src="https://via.placeholder.com/88x88">
																</div>
																<div class="comment-content">
																	<div class="meta">
																		<h6 class="fn">Owen Christ</h6>
																	</div>
																	<div class="comment-text">
																		<p>Thanks for always keeping your HTML Template up to date. Your level of support
																			and dedication is second to none.</p>
																	</div>

																	<div class="comment-actions">
																		<div class="comment-datetime"> 20 de Janeiro de 2021 </div>
																	</div>
																</div>
															</div>
														</li><!-- comment End-->
														<li class="comment">
															<div class="comment-5">
																<div class="comment-author vcard">
																	<img alt="" src="https://via.placeholder.com/88x88">
																</div>
																<div class="comment-content">
																	<div class="meta">
																		<h6 class="fn">James Scott</h6>
																		<div class="comment-datetime">
																			November 13, 2018 at 4:50 am </div>
																	</div>
																	<div class="comment-text">
																		<p>Thanks for always keeping your HTML Template up to date. Your level of support
																			and dedication is second to none.</p>
																	</div>

																	<div class="comment-actions">
																		<div class="comment-datetime"> 20 de Janeiro de 2021 </div>
																	</div>
																</div>
															</div>
															<ol class="children">
																<li class="comment ">
																	<div class="comment-6">
																		<div class="comment-author vcard">
																			<img alt="" src="https://via.placeholder.com/88x88">
																		</div>
																		<div class="comment-content">
																			<div class="meta">
																				<h6 class="fn">Harry Ferguson</h6>
																				<div class="comment-datetime">
																					February 13, 2019 at 4:51 am </div>
																			</div>
																			<div class="comment-text">
																				<p>Thanks for always keeping your HTML Template up to date. Your level of
																					support and dedication is second to none.</p>
																			</div>

																			<div class="comment-actions">
																				<div class="comment-datetime"> 20 de Janeiro de 2021 </div>
																			</div>
																		</div>
																	</div>
																</li><!-- comment End -->
															</ol><!-- children End -->
														</li><!-- comment End-->
													</ol>
												</div>
											</div>
										</div>

										<div class="comment-list-wrapper">

											<div class="row">

												<div class="col-lg-12">
													<h4 class="widget-title mb-20">Deixe aqui o seu comentário </h4>
												</div>

												<div class="col-lg-12">
													<div class="contact-from-wrapper section-space--mt_30">
														<form action="#" method="post">
															<div class="contact-page-form">
																<div class="contact-input">
																	<div class="contact-inner">
																		<label for="name">Nome*</label>
																		<input name="name" type="text">
																	</div>
																	<div class="contact-inner">
																		<label for="email">E-mail*</label>
																		<input name="email" type="email">
																	</div>

																</div>
																<div class="contact-inner contact-message">
																	<label for="mensagem">Mensagem</label>
																	<textarea name="comment"></textarea>
																</div>
																<div class="comment-submit-btn">
																	<button class="ht-btn ht-btn-md bg-spotlight" type="submit">ENVIAR COMENTÁRIO <i class="far fa-chevron-circle-right"></i></button>
																</div>
															</div>
														</form>
													</div>
												</div>
											</div>
										</div>
									</div>

								</div>
								<!-- Post info End -->
							</div>
							<!--===== Single Blog Item End =========-->

						</div>
					</div>
				</div>
			</div>
		</div>
		<!--====================  Blog Area End  ====================-->


	</div>

	<?php include('partials/newsletter.php'); ?>
	<?php include_once('footer.php'); ?>
